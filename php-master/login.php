<?php session_start();  ?>
<!DOCTYPE HTML>
<html lang="pt-br" class="uk-height-1-2">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login</title>
		<link rel="icon" href="imgs/money.png">
		<link rel="stylesheet" type="text/css" href="css/uikit.min.css">
		<link rel="stylesheet" type="text/css" href="css/alertify.min.css">
		<link rel="stylesheet" href="css/themes/default.min.css" />
		<link rel="stylesheet" type="text/css" href="css/login.css">
	</head>
		
	<?php if(isset($_SESSION['usuario'])){
			header("Location: dashboard.php");
	}  
	?>

	<body class="uk-height-1-1 uk-cover-container uk-background-secondary" id="login">
		<div class="uk-position-cover uk-overlay-primary"></div>
		
		<div class="uk-flex uk-flex-center uk-flex-middle uk-height-viewport uk-light uk-position-relative uk-position-z-index">
			<div class="uk-position-bottom-center uk-position-small uk-visible@m">
			</div>
			<div class="uk-width-medium uk-padding-small" uk-scrollspy="cls: uk-animation-fade">
				
				<!-- FORMULARIO DE LOGIN -->
				<h1>Entrar</h1>
				<form action="loginBack.php" method="post">
					<fieldset class="uk-fieldset">
						<div class="uk-margin">
							<div class="uk-inline uk-width-1-1">
								<span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: mail"></span>
								<input name="email" class="uk-input uk-border-pill" type="email" required placeholder="Email do Usuario">
							</div>
						</div>
						<div class="uk-margin">
							<div class="uk-inline uk-width-1-1">
								<span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: lock"></span>
								<input name="senha" class="uk-input uk-border-pill" required placeholder="Senha do Usuario" type="password">
							</div>
						</div>
						<div class="uk-margin">
							<button type="submit" class="uk-button uk-button-primary uk-border-pill uk-width-1-1">Entre</button>
						</div>
					</fieldset>

					<!-- FORMULARIO DE REGISTRO -->
				</form>
				<div>
					<div class="uk-text-center">
						<a class="uk-link-reset uk-text-small" data-uk-toggle="target: #recover;animation: uk-animation-slide-top-small">Não possui uma conta? Deseja registrar?</a>
					</div>
					<div class="uk-margin-small-top" id="recover" hidden>
						<form action="cadastrar" method="post" id="form">
							<h1>Registro</h1>
							<div class="uk-margin-small">
								<div class="uk-inline uk-width-1-1">
									<span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: user"></span>
									<input class="uk-input uk-border-pill validarBotao" placeholder="Nome do Usuario"  type="text" name="nome" id="nome">
								</div>
							</div>
							<span id="spanNome"></span>
							<div class="uk-margin-small">
								<div class="uk-inline uk-width-1-1">
									<span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: mail"></span>
									<input class="uk-input uk-border-pill validarBotao" placeholder="E-mail"  type="text" name="email" id="email">
								</div>
							</div>
							<span id="spanEmail"></span>
							<div class="uk-margin-small">
								<div class="uk-inline uk-width-1-1">
									<span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: lock"></span>
									<input class="uk-input uk-border-pill validarBotao" placeholder="Senha"  type="password" name="senha" id="senha">
								</div>
							</div>
							<span id="spanSenha"></span>
							<div class="uk-margin-small">
								<div class="uk-inline uk-width-1-1">
									<span class="uk-form-icon uk-form-icon-flip" data-uk-icon="icon: lock"></span>
									<input class="uk-input uk-border-pill validarBotao" placeholder="Confirmar Senha"  type="password" name="senha2" id="senha2">
								</div>
							</div>
							<span id="spanSenha2"></span>
							<div class="uk-margin-small">
								<button type="submit" class="uk-button uk-button-primary uk-border-pill uk-width-1-1" id="botao">Registrar</button>
							</div>	
						</form>
					</div>
				</div>
			</div>
		</div>
		<script src="js/jquery-3.3.1.min.js"></script>
		<script src="js/ajax.js"></script>
		<script src="js/alertify.min.js"></script>
		<script src="js/uikit.min.js"></script>
		<script src="js/uikit-icons.min.js"></script>
		<script src="js/script.js"></script>
	</body>
	
</html>
