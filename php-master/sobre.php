<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sobre</title>
<link rel="icon" href="imgs/money.png">
<link rel="stylesheet" type="text/css" href="css/uikit.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<?php

		if(isset($_SESSION['usuario'])){
			header("Location: dashboard.php");
		}

	  ?>
	<div class="top-wrap uk-position-relative uk-light uk-background-secondary">
			
			
			<div class="nav" data-uk-sticky="cls-active: uk-background-secondary uk-box-shadow-medium; top: 100vh; animation: uk-animation-slide-top">
				<div class="uk-container">
					<nav class="uk-navbar-container uk-navbar-transparent" data-uk-navbar>
						<div class="uk-navbar-left">
							<div class="uk-navbar-item uk-padding-remove-horizontal">
								<a class="uk-logo" title="Logo" href="index.php"><h2>yourMoney</h2></a>
							</div>
						</div>
						<div class="uk-navbar-right">
							<ul class="uk-navbar-nav uk-visible@s">
								<li class="uk-active uk-visible@m"><a href="index.php" data-uk-icon="home"></a></li>
								<li><a href="login.php">Entrar/Cadastrar</a></li>
								<li><a href="sobre.php">Sobre</a></li>

							</ul>
							<a class="uk-navbar-toggle uk-navbar-item uk-hidden@s" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav"></a>
						</div>
					</nav>
				</div>
			</div>
			
			
			<div class="uk-cover-container uk-light uk-flex uk-flex-middle top-wrap-height">
		
				
				<div class="uk-container uk-flex-auto top-container uk-position-relative uk-margin-medium-top" data-uk-parallax="y: 0,50; easing:0; opacity:0.2">
					<div class="uk-width-1-2@s" data-uk-scrollspy="cls: uk-animation-slide-right-medium; target: > *; delay: 150">
						<h1 class="uk-margin-remove-top">Sobre</h1>
						<p class="subtitle-text uk-visible@s">Com <i>yourMoney</i> você pode administrar as suas transações, permitindo você facilitar a sua vida.</p>
					</div>
				</div>
			
			
			<img src="imgs/aa.jpg"
				data-sizes="100vw"
				data-src="imgs/aa.jpg" alt="" data-uk-cover data-uk-img data-uk-parallax="opacity: 1,0.1; easing:0"
				>	
				
			</div>
		
		</div>

		<section id="content" class="uk-section uk-section-default">
			<div class="uk-container">
				<div class="uk-section uk-section-small uk-padding-remove-top">
					<ul class="uk-subnav uk-subnav-pill uk-flex uk-flex-center" data-uk-switcher="connect: .uk-switcher; animation: uk-animation-fade">
						<li><a class="uk-border-pill" href="#">Sistema</a></li>
						<li><a class="uk-border-pill" href="#">Benefícios</a></li>
						<li><a class="uk-border-pill" href="#">Estatisticas</a></li>
					</ul>
				</div>

				<ul class="uk-switcher uk-margin">
					<li>
						<div class="uk-grid uk-child-width-1-2@l uk-flex-middle" data-uk-grid data-uk-scrollspy="target: > div; cls: uk-animation-slide-left-medium">
							<div>
								<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="imgs/marketing-1.svg" alt="" data-uk-img>
							</div>
							<div data-uk-scrollspy-class="uk-animation-slide-right-medium">
								<h2 class="uk-margin-small-top">Tome decisões com dados em tempo real com base na interação dos usuários.</h2>
								<p class="subtitle-text">
									Nosso sistema realiza multitarefas para nossos úsuarios com simples cliques.
								</p>
								<div class="uk-grid uk-child-width-1-2@s" data-uk-grid>
									<div>
										<h4>Registros</h4>
										<p>Adição de multiplos registros a qualquer momento.</p>
									</div>
									<div>
										<h4>Datas</h4>
										<p>Datas e Horários de cada registro</p>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li>
						<div class="uk-grid uk-child-width-1-2@l uk-flex-middle" data-uk-grid data-uk-scrollspy="target: > div; cls: uk-animation-slide-left-medium">
							<div>
								<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="imgs/marketing-8.svg" alt="" data-uk-img>
							</div>
							<div data-uk-scrollspy-class="uk-animation-slide-right-medium">
								<h2 class="uk-margin-small-top">Usar nosso sistema aonde você estiver</h2>
								<p class="subtitle-text">
									Nós disponibilizamos para você uma aplicação que pode ser usado onde você estiver
								</p>
								
							</div>
						</div>
					</li>
					<li>
						<div class="uk-grid uk-child-width-1-2@l uk-flex-middle" data-uk-grid data-uk-scrollspy="target: > div; cls: uk-animation-slide-left-medium">
							<div>
								<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="imgs/marketing-9.svg" alt="" data-uk-img>
							</div>
							<div data-uk-scrollspy-class="uk-animation-slide-right-medium">
								<h2 class="uk-margin-small-top">Estatíscas em Tempo real.</h2>
								<p class="subtitle-text">
									O nosso sistema realiza calculos simples e rápidos.
								</p>
								
							</div>
						</div>
					</li>
				</ul>
				
				
			</div>
		</section>

		<footer class="uk-section uk-section-secondary uk-padding-remove-bottom">
			<div class="uk-container">
				<div class="uk-grid uk-grid-large" data-uk-grid>
					<div class="uk-width-1-2@m">
						<h5>CONTATOS</h5>
						<p></p>
						<div>
							<a href="" class="uk-icon-button" data-uk-icon="twitter"></a>Twitter<br><br>
							<a href="" class="uk-icon-button" data-uk-icon="facebook"></a>Facebook<br><br>
							<a href="" class="uk-icon-button" data-uk-icon="instagram"></a>Instagram
						</div>
					</div>
					<div class="uk-width-1-6@m">
						<h5>PÁGINAS</h5>
						<ul class="uk-list">
							<li>Entrar</li>
							<li>Cadastrar</li>
							<li>Sobre</li>
						</ul>
					</div>
					
					
				</div>
			</div>
			
			<div class="uk-text-center uk-padding uk-padding-remove-horizontal">
				<span class="uk-text-small uk-text-muted">© 2019 yourMoney - <a href="">Desenvolvido por Lucas Silva, Lucas Wendel e Rodrigo Silva</a> | Feito com <a href="http://getuikit.com" title="Visit UIkit 3 site" target="_blank" data-uk-tooltip><span data-uk-icon="uikit"></span></a></span>
			</div>
		</footer>
		
</body>
	
		<script src="js/uikit.min.js"></script>
		<script src="js/uikit-icons.min.js"></script>
</html>