<?php
	require_once("Modelo/ContaBancaria.class.php");
	require_once("Modelo/Cartao.class.php");
	class Registro{
		private $id;
		private $titulo;
		private $valor;
		private $descricao;
		private $repeticao;
		private $tipoCartao;
		private $tipoPagamento;
		private $tipoRegistro;
		private $dataRegistro;
		private $dataOperacao;
		private $dataVencimento;
		private $conta;
		private $cartao;

		public function getId(){
			return $this->id;
		}
		public function setId($i){
			$this->id = (isset($i)) ? $i :NULL;
		}
		public function getTitulo(){
			return $this->titulo;
		}
		public function setTitulo($t){
			$this->titulo = (isset($t)) ? $t :NULL;
		}
		public function getValor(){
			return $this->valor;
		}
		public function setValor($v){
			$this->valor = (isset($v)) ? $v :NULL;
		}
		public function getDesc(){
			return $this->descricao;
		}
		public function setDesc($desc){
			$this->descricao = (isset($desc)) ? $desc :NULL;
		}
		public function getRepeticao(){
			return $this->repeticao;
		}
		public function setRepeticao($repeticao){
			$this->repeticao = (isset($repeticao)) ? $repeticao :NULL;
		}
		public function getTipoCartao(){
			return $this->tipoCartao;
		}
		public function setTipoCartao($tC){
			$this->tipoCartao = (isset($tC)) ? $tC :NULL;
		}
		public function getTipoPagamento(){
			return $this->tipoPagamento;
		}
		public function setTipoPagamento($tP){
			$this->tipoPagamento = (isset($tP)) ? $tP :NULL;
		}
		public function getTipoRegistro(){
			return $this->tipoRegistro;
		}
		public function setTipoRegistro($tR){
			$this->tipoRegistro = (isset($tR)) ? $tR :NULL;
		}
		public function getDataRegistro(){
			return $this->dataRegistro;
		}
		public function setDataRegistro($dR){
			$this->dataRegistro = (isset($dR)) ? $dR :NULL;
		}
		public function getDataOperacao(){
			return $this->dataOperacao;
		}
		public function setDataOperacao($dO){
			$this->dataOperacao = (isset($dO)) ? $dO :NULL;
		}
		public function getDataVencimento(){
			return $this->dataVencimento;
		}
		public function setDataVencimento($dV){
			$this->dataVencimento = (isset($dV)) ? $dV :NULL;
		}
		public function getConta(){
			return $this->conta;
		}
		public function setConta($conta){
			$this->conta = $conta;
		}
		public function getCartao(){
			return $this->cartao;
		}
		public function setCartao($cartao){
			$this->cartao = $cartao;
		}

		function __construct(){
			$this->conta = new ContaBancaria();
			$this->cartao = new Cartao();
		}
	}
  ?>