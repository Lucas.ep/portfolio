<?php

class Usuario {
	private $id;
	private $nome;
	private $email;
	private $senha;
	private $carteira;

	public function getId(){
		return $this->id;
	}
	public function setId($i){
		$this->id = (isset($i)) ? $i :NULL;
	}
	public function getNome(){
		return $this->nome;
	}
	public function setNome($nome){
		$this->nome = (isset($nome)) ? $nome :NULL;
	}
	public function getEmail(){
		return $this->email;
	}
	public function setEmail($email){
		$this->email = (isset($email)) ? $email :NULL;
	}
	public function getSenha(){
		return $this->senha;
	}
	public function setSenha($senha){
		$this->senha = (isset($senha)) ? $senha : NULL;
	}
	public function getCarteira(){
		return $this->carteira;
	}
	public function setCarteira($carteira){
		$this->carteira = (isset($carteira)) ?$carteira :NULL;
	}
}

  ?>