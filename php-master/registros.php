
<?php
	require_once('Controle/CartaoController.class.php');
	require_once('Controle/ContaBancariaController.class.php');
	require_once('Controle/RegistroController.class.php');
	require_once('Controle/UsuarioController.class.php');

  ?>    
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Meus registros</title>
		<link rel="icon" href="imgs/money.png">
		<link rel="stylesheet" type="text/css" href="css/uikit.min.css">
		<link rel="stylesheet" type="text/css" href="css/dashboard.css">
		<link rel="stylesheet" type="text/css" href="css/alertify.min.css">
	</head>
	<body>
	
	 	
		<?php
		session_start();
		 if(!isset($_SESSION["usuario"])){
			header("Location: index.php");

		}	
		setcookie("addRegistroResult",null);
		date_default_timezone_set("America/Fortaleza");
		$data = new DateTime();
		$usu = unserialize($_SESSION['usuario']);
		?>
		<?php

			$registroControle = new RegistroController();
			$regList = $registroControle->consultarRegistros(unserialize($_SESSION['usuario'])->getId());

			$contaControle = new ContaBancariaController();
			$contaList = $contaControle->consultarContasBancarias(unserialize($_SESSION['usuario'])); 

			$cartaoControle = new CartaoController();
			$cartaoList = $cartaoControle->consultarCartoes(unserialize($_SESSION['usuario'])); 
			
			$detalhes = [];
		?>
		<header id="top-head" class="uk-position-fixed">
			<div class="uk-container uk-container-expand " style="background-color: #222;">
				<nav class="uk-navbar uk-light" data-uk-navbar="mode:click; duration: 250">
					<div class="uk-navbar-left">
						<img style='width:30px' src='imgs/money.png'> 
						<a href="dashboard.php">yourMoney</a>
					</div>
					<div class="uk-navbar-right">
						<ul class="uk-navbar-nav">
							<li><a data-uk-icon="icon:  bell" onclick="mostrarAvisos()" title="Avisos" data-uk-tooltip></a></li>
							<li><a href="sair.php" data-uk-icon="icon:  sign-out" title="Sair" data-uk-tooltip></a></li>
							<li><a class="uk-navbar-toggle" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav" title="Offcanvas" data-uk-tooltip></a></li>
						</ul>
					</div>
				</nav>
			</div>
		</header>
		
		<div id="content" data-uk-height-viewport="expand: true">
			<div class="uk-container uk-container-expand">
				<div class="uk-grid uk-grid-divider uk-grid-medium uk-child-width-1-1 uk-child-width-1-4@l uk-child-width-1-5@xl" data-uk-grid>
					<div>
						<span class="uk-text-small"><span class="uk-margin-small-right uk-text-primary" style="font-size:17px">$</span>Carteira</span>
						
							<?php
								$uC=new UsuarioController();
								$a=$uC->mostrarCarteira(unserialize($_SESSION['usuario'])->getId());
								if($a[0]->getCarteira()==0.00){
									echo"
									<div class='uk-text-small'>
									Você não possui.
									<h2 class='uk-heading-secondary uk-margin-remove uk-text-primary'><button class='uk-button uk-button-secondary' id='botaoCarteira'>Adicionar</button></h2>";
								}else{
									$c=number_format($a[0]->getCarteira(),2,",",".");
									echo "
									<h1 class='uk-heading-primary uk-margin-remove  uk-text-primary' style='font-size:150%;'>R$ {$c}</h1>
									<div class='uk-text-small'>";
									
								}
							  ?>	

						</div>
						
					</div>
					<div>
						<span class="uk-text-small"><span data-uk-icon="icon:bookmark" class="uk-margin-small-right uk-text-primary"></span>Registros</span>
						
						<h2 class="uk-heading-secondary uk-margin-remove uk-text-primary"><button id="botaoRegistro" class="uk-button uk-button-secondary">Adicionar</button></h2>
					</div>
					
				</div>
				<hr>
				<div class="uk-grid uk-grid-medium" data-uk-grid>
					
					<div class="uk-width-2-3@l">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Meus registros</h4></div>
									
								</div>
							</div>
							<div class="uk-card-body uk-overflow-auto" style='max-height:500px'>
							
								
								<table class="uk-table uk-table-divider uk-table-striped uk-table-hover" >
									<tr>
										<th></th>
										<th>Título</th>
										<th>Valor</th>
										<th>Meio</th>
										<th>Data</th>
										<th></th>
									</tr>
									<tbody id='recebeFiltro' >
									<?php
											$cont = 0;
											
											foreach ($regList as $rL) {
												$titulo = $rL->getTitulo();
												$valor = $rL->getValor();
												$tpRegistro = "plus.png";
												$tipoRegistro = "Entrada";
												if($rL->getTipoRegistro()=="saida"){
													$valor = $valor*(-1);
													$tpRegistro = "less.png";
													$tipoRegistro = "Saída";
												}
												$valorNormal = $valor;
												$valor = number_format($valor, 2, ",", ".");

												$idForma = $usu->getId();
												$meioDetalhado = "";
												if($rL->getTipoPagamento()=="carteira"){
													$meio = "Carteira";
													$meioDetalhado = "Carteira";
												}else if($rL->getTipoPagamento()=="conta"){
													$meio = "Conta Bancaria";
													$meioDetalhado = "Conta Bancaria ".$rL->getConta()->getConta()." - ".$rL->getConta()->getAgencia()->getNome();
													$idForma = $rL->getConta()->getConta();
												}else{
													$meio = "Cartão";
													if($rL->getCartao()->getContaBancaria()->getConta()!=null){
														$meioDetalhado = "Cartão ".$rL->getCartao()->getId()." - ".$rL->getCartao()->getContaBancaria()->getAgencia()->getNome();
													}else{
														$meioDetalhado = "Cartão ".$rL->getCartao()->getId()." - Sem Banco";
													}
													
													$idForma = $rL->getCartao()->getId();
												}
												$dataReg = date('d-m-Y à\s H:i:s',strtotime($rL->getDataRegistro()));
												$dataOp = date('d-m-Y',strtotime($rL->getDataOperacao()));
												$dataVen = date('d-m-Y',strtotime($rL->getDataVencimento()));
												$dataVen = ($rL->getDataVencimento()!=null) ?"Dia de Vencimento: ".date('d-m',strtotime($rL->getDataVencimento()))." <> Até ".$dataVen :"";
												echo "<tr>";
												echo "<td><img style='width:15px;height:15px' src='imgs/{$tpRegistro}'></td>";
												echo "<td>{$titulo}</td>";
												echo "<td>R$ {$valor}</td>";
												echo "<td>{$meio}</td>";
												echo "<td>{$dataReg}</td>";
												echo "<td><a class='uk-icon-button verRegistro' onclick='verRegistro({$cont})'><span uk-icon='icon: search'></span></a>";
												echo "<a onclick='deletarRegistro({$rL->getId()},{$rL->getTipoPagamento()},{$valorNormal},{$idForma})' class='uk-icon-button'><span uk-icon='icon: close'></span></a></td>";
												echo "</tr>";

				

												$detalhes[$cont] = "".
												"<h2>{$titulo} <a class='uk-icon-button btnPDF' id='{$cont}'><span uk-icon='icon: download'></span></a></h2>".
												"<hr></hr>".
												"<p>Valor: R$ {$valor}</p>".
												"<p>Tipo de registro: {$tipoRegistro}</p>".
												"<p>Meio: {$meioDetalhado}</p>".
												"<p>Data de registro: {$dataReg}</p>".
												"<p>Data de operação: {$dataOp}</p>".
												"<p>{$dataVen}</p>".
												"<hr class='uk-divider-icon'></hr>".
												"<p>{$rL->getDesc()}</p>";
												
												
												$cont+=1;
											}
											
											$detalhesString = implode("|", $detalhes);
											$qntdReg = count($regList);
										?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

					<div class="uk-width-1-3@l">
						<div class="uk-card uk-card-default uk-card-small uk-card-hover">
							<div class="uk-card-header">
								<div class="uk-grid uk-grid-small">
									<div class="uk-width-auto"><h4>Filtros</h4></div>
								</div>
							</div>
							<div class="uk-card-body">
								<form id='filtroForm'>
									<label>Filtrar..</label>
									<select class="uk-select" id='filtro' name='filtro' onchange='mudarFiltro()'>
										<option value='nenhum'>Todos</option>
										<option value='titulo'>Por título</option>
										<option value='valor'>Por valor</option>
										<option value='tipoPagamento'>Por meio</option>
										<option value='tipoRegistro'>Por tipo de registro</option>
										<option value='dataOperacao'>Por data de operação</option>
										<option value='dataRegistro'>Por data de registro</option>
										<option value='dataVencimento'>Por data de vencimento</option>
									</select><br>
									<div id='tituloField' style='display:none'><label>Título</label><br><input class='uk-input' type='text' id='titulo' name='titulo'  placeholder='Título'></div>
									<div id='valorField' style='display:none'><label>Valor</label><br><input class='uk-input' type='text' id='valor' name='valor' placeholder='Valor'></div>
									<div id="tipoFieldP" style='display:none'><label>Tipo de Pagamento</label><br><select class='uk-select' type='text' id='tipoPagamento' name='tipoPagamento'><option value='carteira'>Carteira</option><option value='conta'>Conta Bancária</option><option value='cartao'>Cartão de Crédito</option></select></div>
									<div id="tipoFieldR" style='display:none'><label>Tipo de Registro</label><br><select class='uk-select' type='text' id='tipoRegistro' name='tipoRegistro'><option value='saida'>Saída</option><option value='entrada'>Entrada</option></select></div>
									<div id="dataFieldO" style='display:none'><label>Data de operação</label><br><input class='uk-input' type='date' id='dataOperacao' name='dataOperacao'></div>
									<div id="dataFieldR" style='display:none'><label>Data de Registro</label><br><input class='uk-input' type='date' id='dataRegistro' name='dataRegistro'></div>
									<div id="dataFieldV" style='display:none'><label>Data de Vencimento</label><br><input class='uk-input' type='date' id='dataVencimento' name='dataVencimento'></div><br>
									<input id='idUsu' type='hidden' value='<?php echo $usu->getId() ?>'>
									<input class='uk-button uk-button-secondary' type='submit' value='Filtrar'>
								</form>
							</div>
						</div>
					</div>
		
				</div>
				<footer class="uk-section uk-section-small uk-text-center">
					<hr>
					<p class="uk-text-small uk-text-center">© 2019 yourMoney - <a href="">Desenvolvido por Lucas Silva, Lucas Wendel e Rodrigo Silva</a> | Feito com <a href="http://getuikit.com" title="Visit UIkit 3 site" target="_blank" data-uk-tooltip><span data-uk-icon="uikit"></span></a> </p>
				</footer>
			</div>
		</div>

		<div id="offcanvas-nav" data-uk-offcanvas="flip: true; overlay: true">
			<div class="uk-offcanvas-bar uk-offcanvas-bar-animation uk-offcanvas-slide">
				<button class="uk-offcanvas-close uk-close uk-icon" type="button" data-uk-close></button>
				<aside id="left-col" class="uk-light ">
					<div class="left-logo uk-flex uk-flex-middle">
						<img class="custom-logo" src="imgs/money.png" alt="yourMoney"> yourMoney
					</div>
					<div class="left-content-box  content-box-dark">
						<h4 class="uk-text-center uk-margin-remove-vertical text-light"></h4>
						
						<div class="uk-position-relative uk-text-center uk-display-block">
						    <a href="#" class="uk-text-small uk-text-muted uk-display-block uk-text-center" data-uk-icon="icon: triangle-down; ratio: 0.7"><?php echo $usu->getNome() ?></a>
						   
						    <div class="uk-dropdown user-drop" data-uk-dropdown="mode: click; pos: bottom-center; animation: uk-animation-slide-bottom-small; duration: 150">
						    	<ul class="uk-nav uk-dropdown-nav uk-text-left">
										<li><a href="sair.php"><span data-uk-icon="icon: sign-out"></span> Sair</a></li>
							    </ul>
						    </div>
						   
						</div>
					</div>
					
					<div class="left-nav-wrap">
						<ul class="uk-nav uk-nav-default uk-nav-parent-icon" data-uk-nav>
							<li class="uk-nav-header">PÁGINAS</li>
							<li><a href="dashboard.php"><span data-uk-icon="icon: home" class="uk-margin-small-right"></span>Início</a></li>
							<li><a href="registros.php"><span data-uk-icon="icon: bookmark" class="uk-margin-small-right"></span>Registros</a></li>
							<li><a href="contas.php"><span data-uk-icon="icon: world" class="uk-margin-small-right"></span>Contas Bancárias</a></li>
							<li><a href="cartoes.php"><span data-uk-icon="icon: credit-card" class="uk-margin-small-right"></span>Cartões de Crédito</a></li>
							
						</ul>
						<div class="left-content-box uk-margin-top">
							
								<h5>Sobre o <i>yourMoney</i></h5>
								Com <i>yourMoney</i>, você pode administrar as suas transações, permitindo você facilitar a sua vida.
							
						</div>
						
					</div>
					
				</aside>
			</div>
		</div>
	
		<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
		<script src="js/uikit.min.js"></script>
		<script src="js/uikit-icons.min.js"></script>
		
		<script src="js/alertify.min.js"></script>
		<script type="text/javascript" src="js/input-mask.js"></script>
		<script type="text/javascript" src="js/maskMoney.min.js"></script>
		
		<?php 
			$contas = "";
			for($i=0;$i<count($contaList);$i++){
				$nConta = $contaList[$i]->getConta();
				$agenciaConta = $contaList[$i]->getAgencia()->getNome(); 
				$contas = $contas . "<option value='{$nConta}'>{$agenciaConta} - {$nConta}</option>";
			}

			$cartoes = "";
			for($i=0;$i<count($cartaoList);$i++){
				$nCartao = substr($cartaoList[$i]->getId(),-4,4);
				$nCartao = "**** **** **** {$nCartao}";
				$apenasDebito = "";
				$agencia = ($cartaoList[$i]->getContaBancaria()->getConta()!=null) ?$cartaoList[$i]->getContaBancaria()->getAgencia()->getNome() : "Sem Banco";
				if($cartaoList[$i]->getLimite()==null || $cartaoList[$i]->getContaBancaria()->getConta()==null){
					$apenasDebito = "class='opcoesCredito'";
				}
				$cartoes = $cartoes . "<option {$apenasDebito} value='{$cartaoList[$i]->getId()}'>{$agencia} - {$nCartao}</option>";
			}

			$registrosFixos = "";
			for($i=0;$i<count($regList);$i++){
				if($regList[$i]->getRepeticao()=="fixa" && $regList[$i]->getDataVencimento()!=null){
					$idReg = $regList[$i]->getId();
					$titulo = $regList[$i]->getTitulo();
					$registrosFixos = $registrosFixos . "<option value='{$idReg}'>{$titulo}</option>";
				}
			}

			$parcelamentos = "";
			for($i=0;$i<count($regList);$i++){
				if($regList[$i]->getRepeticao()=="parcelamento" && $regList[$i]->getDataVencimento()!=null){
					$idReg = $regList[$i]->getId();
					$titulo = $regList[$i]->getTitulo();
					$parcelamentos = $parcelamentos . "<option value='{$idReg}'>{$titulo}</option>";
				}
			}

			$avisos = "<div class='uk-grid-small' uk-grid>";
			$avisosPrioridadeAlta = "";
			$avisosPrioridadeMedia = "";
			$avisosPrioridadeBaixa = "";
			$countAvisos = 0;
			foreach ($regList as $rL) {
				if($rL->getDataVencimento()){
					$dtVenReg = null;
					if($rL->getRepeticao()=="fixa"){
						$dtVenReg = new DateTime($rL->getDataVencimento());
					}else{
						$dtVenReg = new DateTime($rL->getDataOperacao());
						$dtVenReg->modify("+1 month");
					}
					$difDatas = $dtVenReg->diff($data);
					if($difDatas->invert==0 && $difDatas->days!=0){
						$avisosPrioridadeAlta.= "".
						"<div class='uk-width-1-1'>".
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>".
								"<div class='uk-card-header'>".
									"<div class='uk-grid uk-grid-small'>".
										"<div class='uk-width-auto'><h4>Pagamento Vencido: {$rL->getTitulo()}</h4></div>".
									"</div>".
								"</div>".
								"<div class='uk-card-body'>".
									"Você possui um pagamento vencido referente ao dia <b>{$dtVenReg->format('d-m-Y')}</b> !".
								"</div>".
							"</div>".
						"</div>";
						$countAvisos++;
					}else if($difDatas->days<1 && (integer)$dtVenReg->format("d")!=(integer)$data->format("d")+1){
						$avisosPrioridadeMedia.= "".
						"<div class='uk-width-1-1'>".
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>".
								"<div class='uk-card-header'>".
									"<div class='uk-grid uk-grid-small'>".
										"<div class='uk-width-auto'><h4>A vencer hoje: {$rL->getTitulo()}</h4></div>".
									"</div>".
								"</div>".
								"<div class='uk-card-body'>".
									"Um pagamento vence hoje <b>{$dtVenReg->format('d-m-Y')}</b>! ".
								"</div>".
							"</div>".
						"</div>";
						$countAvisos++;
					}else if($difDatas->days<7){
						$avisosPrioridadeBaixa.= "".
						"<div class='uk-width-1-1'>".
							"<div class='uk-card uk-card-primary uk-card-small uk-card-hover'>".
								"<div class='uk-card-header'>".
									"<div class='uk-grid uk-grid-small'>".
										"<div class='uk-width-auto'><h4>Vencimento próximo: {$rL->getTitulo()}</h4></div>".
									"</div>".
								"</div>".
								"<div class='uk-card-body'>".
									"Um pagamento está próximo de seu vencimento, que está previsto para o dia <b>{$dtVenReg->format('d-m-Y')}</b>.".
								"</div>".
							"</div>".
						"</div>";
						$countAvisos++;
					}
			
				}
			}
			if($countAvisos==0){
				$avisos.="Nenhum aviso por agora! :)";
			}
			$avisos.= $avisosPrioridadeAlta . $avisosPrioridadeMedia . $avisosPrioridadeBaixa;
			$avisos.= "</div>";
		?>

		<script>
			var contasCartao = "";
			var contas = "<?php echo $contas ?>";
			var cartoes = "<?php echo $cartoes ?>";
			var parcelamentos = "<?php echo $parcelamentos ?>";
			var registrosFixos = "<?php echo $registrosFixos ?>";
			var detalhesString = "<?php echo $detalhesString ?>";
			var detalhes = detalhesString.split("|");

			var avisos = "<?php echo $avisos ?>";
			
		</script>
		<script src="js/jspdf.min.js"></script>
		<script src="js/html2canvas.min.js"></script>
		<script type='text/javascript'>
			
			function savePDF(codigoHTML) {
			  var doc = new jsPDF('portrait', 'pt', 'a4'),
			      data = new Date();
			  margins = {
			    top: 40,
			    bottom: 60,
			    left: 40,
			    width: 1000
			  };
			  doc.fromHTML(codigoHTML,
			               margins.left, // x coord
			               margins.top, { pagesplit: true },
			               function(dispose){
			    doc.save("registro - "+data.getDate()+"/"+(data.getMonth()+1)+"/"+data.getFullYear()+".pdf");
			  });
			}
			$('.verRegistro').click(function(){
		        $(".btnPDF").click(function(event) {
		        	var det = detalhes[$(this).attr('id')];
		        	var detalhesPDF = "<img src='imgs/money.png' width='40px'><h1>yourMoney</h1><hr>"+det;
		            savePDF(detalhesPDF);
		            alertify.closeAll();
		        });
		    });

		    
		</script>
		<script src="js/addForms.js"></script>
		<script src="js/javinha.js"></script>
		<script>
		var botRegistro = document.getElementById("botaoRegistro");
		if(!alertify.myAlert){
			alertify.dialog('myAlert',function(){
			return{
				main:function(message){
			        this.message = message;
			      },
			      setup:function(){
			          return { 
			            focus: { element:0 },
		                options:{
		                    maximizable:false,
		                    resizable:true,
		                    movable:false,
		                }
			          };
			      },
			      prepare:function(){
			        this.setContent(this.message);
			      }
			  }});
			}
		botRegistro.onclick = function(){
			alertify.myAlert(formRegistro).resizeTo('40%','80%').setHeader('<h2>Adicionar registro</h2>');
		}
		</script>
		<?php
			if(isset($_COOKIE['addRegistroResult']) ){

				if($_COOKIE['addRegistroResult']=="1"){
					echo "<script>alertify.success('Registro adicionado com sucesso!')</script>";
				}else{
					echo "<script>alertify.error('Erro ao efetuar registro!')</script>";
					echo "<script>alertify.myAlert(formRegistro).resizeTo('40%','80%').setHeader('<h2>Enviar registro</h2>');</script>";	
				}

			}
			
		  ?>
		
	</body>
</html>
