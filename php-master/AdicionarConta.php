<?php
	session_start();
	$numero=$_POST['conta'];
	$valor=(double)$_POST['valor'];
	$tipoConta=$_POST['tipoConta'];
	$agencia=$_POST['agencia'];

	if(isset($numero)){
		try{
			require_once("Modelo/ContaBancaria.class.php");
			require_once("Controle/ContaBancariaController.class.php");
			$conta= new ContaBancaria();
			$conta->setConta($numero);
			$conta->setValor($valor);
			$conta->setTipoConta($tipoConta);
			$conta->setAgencia($agencia);
			$conta->getUsuario()->setId(unserialize($_SESSION['usuario'])->getId());
			$contaC= new ContaBancariaController();
			$contaC->adicionarContaBancaria($conta);
		}catch(Exception $e){
			echo "Error:" .$e->getMessage();
		}
	}
	header("Location: contas.php");
  ?>