<?php

require_once("Conection.class.php");
require_once("Modelo/Usuario.class.php");
require_once("Modelo/Agencia.class.php");
require_once("Controle/AgenciaController.php");
require_once("Modelo/ContaBancaria.class.php");

class ContaBancariaController{
	public function consultarContasBancarias($id){
		$conection=new Conection('lib/mysql.ini');
		$idU=$id->getId();
		$sql="SELECT * FROM  ContaBancaria WHERE id_usuario={$idU}";
		$comando=$conection->getConection()->prepare($sql);
		$comando->execute();
		$list = [];
		if($comando){
			$res = $comando->fetchAll();
			foreach($res as $item){
				$conta = new ContaBancaria();
				$conta->setConta($item->numeroConta);
				$conta->setValor($item->valor);
				$conta->setTipoConta($item->tipoConta);
				$usuario= new Usuario();
				$usuario->setId($id);
				$conta->setUsuario($usuario);
				$aC= new AgenciaController();
				$age = new Agencia();
				$age = $aC->consultarAgencia($item->id_agencia);
				$conta->setAgencia($age);
				array_push($list, $conta);
			}
		}
		$conection-> __destruct();
		return $list;
	}

	public function consultarContasBancariasById($id){
	 	$conection=new Conection('lib/mysql.ini');
	 	$sql="SELECT * FROM  ContaBancaria WHERE numeroConta={$id}";
	 	$comando = $conection->getConection()->prepare($sql);
		$comando->execute();
		$res = $comando->fetch();
		$conta = new ContaBancaria();
		if($res){
			$conta->setConta($res->numeroConta);
			$conta->setValor($res->valor);
			$conta->setTipoConta($res->tipoConta);
			$usuario= new Usuario();
			$usuario->setId($id);
			$conta->setUsuario($usuario);
			$aC= new AgenciaController();
			$age = new Agencia();
			$age = $aC->consultarAgencia($res->id_agencia);
			$conta->setAgencia($age);
		}
		$conection->__destruct();	
		return $conta;
	}

	public function adicionarContaBancaria($conta){
		$conection= new Conection("lib/mysql.ini");
		$numeroConta=$conta->getConta();
		$valor=$conta->getValor();
		$tipoConta=$conta->getTipoConta();
		$agencia=$conta->getAgencia();
		$idUsuario=$conta->getUsuario()->getId();
		$sql="INSERT INTO ContaBancaria(numeroConta,valor,tipoConta,id_agencia,id_usuario) VALUES(:numeroConta,:valor,:tipoConta,:id_agencia,:id_usuario)";
		$comando=$conection->getConection()->prepare($sql);
		$comando->bindParam(":numeroConta",$numeroConta);
		$comando->bindParam(":valor",$valor);
		$comando->bindParam(":tipoConta",$tipoConta);
		$comando->bindParam(":id_agencia",$agencia);
		$comando->bindParam(":id_usuario",$idUsuario);
		if($comando->execute()){
			setcookie("addContaBancaria",1);
		}else{
			setcookie("addContaBancaria",0);
		}
		$conection->__destruct();	
	}

	public function atualizarValor($id,$valor){
		$conection= new Conection("lib/mysql.ini");
		$sql="UPDATE ContaBancaria SET valor=valor+{$valor} WHERE numeroConta={$id}";
		$comando=$conection->getConection()->prepare($sql);

		$comando->execute();
		$conection->__destruct();	
	}


}

  ?>