<?php
require_once("Conection.class.php");
require_once("Controle/ContaBancariaController.class.php");
require_once("Modelo/Usuario.class.php");
require_once("Modelo/Cartao.class.php");

class CartaoController{
	public function adicionarCartao($cartao){
		$connection=new Conection("lib/mysql.ini");
		$idC = $cartao->getId();
		$conta= $cartao->getContaBancaria()->getConta();
		$usuario=$cartao->getUsuario()->getId();
		$valor=($cartao->getLimite()!=null) ?$cartao->getLimite() :0;
		$limite=($cartao->getLimite()!=null) ?$cartao->getLimite() :null;
		$sql="INSERT INTO Cartao (id,valor,limite,id_usuario,id_conta) VALUES(:id,:valor,:limite,:id_usuario,:id_conta)";
		$comando=$connection->getConection()->prepare($sql);
		$comando->bindParam(":id",$idC);
		$comando->bindParam(":valor",$valor);
		$comando->bindParam(":limite",$limite);
		$comando->bindParam(":id_usuario",$usuario);
		$comando->bindParam(":id_conta",$conta);
		if($comando->execute()){
			setcookie("addCartao","1");
		}else{
			echo "SIFUD";	
		}
		$connection->__destruct();
	}	

	public function consultarCartoes($id){
		$conection=new Conection('lib/mysql.ini');
		$idU=$id->getId();
		$sql="SELECT * FROM  Cartao WHERE id_usuario={$idU}";
		$comando=$conection->getConection()->prepare($sql);
		$comando->execute();
		$list = [];
		if($comando){
			$res = $comando->fetchAll();
			foreach($res as $item){
				$cartao = new Cartao();
				$cartao->setId($item->id);
				$cartao->setValor($item->valor);
				$cartao->setLimite($item->limite);
				$usuario= new Usuario();
				$usuario->setId($id);
				$cartao->setUsuario($usuario);
				$cbC= new ContaBancariaController();
				if(isset($item->id_conta)){
					$contB = new ContaBancaria();
					$contB = $cbC->consultarContasBancariasById($item->id_conta);
					$cartao->setContaBancaria($contB);	
				}
				array_push($list, $cartao);
			}
		}
		$conection-> __destruct();
		return $list;
	}

	public function consultarCartoesById($id){
	 	$conection=new Conection('lib/mysql.ini');
	 	$sql="SELECT * FROM  Cartao WHERE id={$id}";
	 	$comando = $conection->getConection()->prepare($sql);
		$comando->execute();
		$res = $comando->fetch();
		$cartao = new Cartao();
		if($res){
			$cartao->setId($res->id);
			$cartao->setValor($res->valor);
			$cartao->setLimite($res->limite);
			$usuario= new Usuario();
			$usuario->setId($res->id_usuario);
			$cartao->setUsuario($usuario);
			if(isset($res->id_conta)){
				$contaC = new ContaBancariaController();
				$conta = $contaC->consultarContasBancariasById($res->id_conta);
				$cartao->setContaBancaria($conta);
			}

		}
		$conection->__destruct();	
		return $cartao;
	}

	public function atualizarValor($id,$valor){
		$conection= new Conection("lib/mysql.ini");
		$sql="UPDATE Cartao SET valor=valor+{$valor} WHERE id={$id}";
		$comando=$conection->getConection()->prepare($sql);

		$comando->execute();
		$conection->__destruct();	
	}

}

?>