<?php
session_start();

require_once("Conection.class.php");
require_once("Modelo/Usuario.class.php");

class UsuarioController{

	public function verificar($user){
	 	$conection=new Conection('lib/mysql.ini');
	 	$email=$user->getEmail();
	 	$senha=$user->getSenha();
	 	$sql="SELECT * FROM  Usuario WHERE email=:email AND senha=:senha;";
	 	$comando = $conection->getConection()->prepare($sql);
		$comando->bindParam(':email', $email);
		$comando->bindParam(':senha', $senha);		
		$comando->execute();
		$users = $comando->fetch();
			if(!$users){
				header("Location: login.php");
			}else{
				$user = new Usuario();
				$user->setId($users->id);
				$user->setNome($users->nome);
				$user->setEmail($users->email);
				$user->setCarteira($users->carteira);
				$_SESSION['usuario']=serialize($user);
				header("Location: dashboard.php");
			}
	
	}
}	
 ?>
