<?php
	session_start();
  ?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>yourMoney</title>
		<link rel="icon" href="imgs/money.png">
		<link rel="stylesheet" type="text/css" href="css/uikit.min.css">
		<link rel="stylesheet" type="text/css" href="css/wowslider.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body> 
		
		<?php if(isset($_SESSION['usuario'])){
				header('Location: dashboard.php');
			}  
		?>
		
		<div class="top-wrap uk-position-relative uk-light uk-background-secondary">
			
			
			<div class="nav" data-uk-sticky="cls-active: uk-background-secondary uk-box-shadow-medium; top: 100vh; animation: uk-animation-slide-top">
				<div class="uk-container">
					<nav class="uk-navbar-container uk-navbar-transparent" data-uk-navbar>
						<div class="uk-navbar-left">
							<div class="uk-navbar-item uk-padding-remove-horizontal">
								<a class="uk-logo" title="Logo" href="index.php"><h2>yourMoney</h2></a>
							</div>
						</div>
						<div class="uk-navbar-right">
							<ul class="uk-navbar-nav uk-visible@s">
								<li class="uk-active uk-visible@m"><a href="" data-uk-icon="home"></a></li>
								<li><a href="login.php">Entrar/Cadastrar</a></li>
								<li><a href="sobre.php">Sobre</a></li>

							</ul>
							<a class="uk-navbar-toggle uk-navbar-item uk-hidden@s" data-uk-toggle data-uk-navbar-toggle-icon href="#offcanvas-nav"></a>
						</div>
					</nav>
				</div>
			</div>
			
			
			<div class="uk-cover-container uk-light uk-flex uk-flex-middle top-wrap-height">
				
				
				<div class="uk-container uk-flex-auto top-container uk-position-relative uk-margin-medium-top" data-uk-parallax="y: 0,50; easing:0; opacity:0.2">
					<div class="uk-width-1-2@s" data-uk-scrollspy="cls: uk-animation-slide-right-medium; target: > *; delay: 150">
						<h1 class="uk-margin-remove-top">Gerencie seu dinheiro com <i>yourMoney</i>.</h1>
						<p class="subtitle-text uk-visible@s">Com <i>yourMoney</i> você pode administrar as suas transações, permitindo você facilitar a sua vida.</p>
						<a href="#" title="Learn More" class="uk-button uk-button-primary uk-border-pill" data-uk-scrollspy-class="uk-animation-fade">Saiba mais</a>
					</div>
				</div>
				
				
				<img src="imgs/fundo.jpg"
				data-sizes="100vw"
				data-src="imgs/fundo.jpg" alt="" data-uk-cover data-uk-img data-uk-parallax="opacity: 1,0.1; easing:0"
				>
				
			</div>
			<div class="uk-position-bottom-center uk-position-medium uk-position-z-index uk-text-center">
				<a href="#wowslider-container1" data-uk-scroll="duration: 800" data-uk-icon="icon: arrow-down; ratio: 2"></a>
			</div>
		</div>
					
			<div id='wowslider-container1' data-no-devices='true' data-fullscreen='true'>
			  	<div id='wowslider-container'>
	                <div class='ws_images'>
	                    <ul>
	                        <li><a href='#'><img src='imgs/d.jpg'  title='Conecte-se com o Mundo' id='wows_0'/></a></li>
	                        <li><a href='#'><img src='imgs/a.jpg'  title='Aulas de Desing' id='wows_0'/></a></li>
	                        <li><a href='#'><img src='imgs/b.jpg'  title='Equipamentos' id='wows_0'/></a></li>
	                	</ul>
	                </div>
	                <div class='ws_bullets'><div>
						<a href='#' ><span><img src='imgs/d.jpg' /></span></a>
						<a href='#' ><span><img src='imgs/a.jpg' /></span></a>
						<a href='#' ><span><img src='imgs/b.jpg' /></span></a>
					</div>
           	   		</div>
           	   		<div class='ws_shadow'></div>
           		</div>	
           	</div>
            			
		<section class="uk-section uk-section-secondary uk-section-large">
			<div class="uk-container">
				<div class="uk-grid uk-child-width-1-2@l uk-flex-middle">
					<div>
						
						<h2 class="uk-margin-small-top uk-h1">SIMPLICIDADE</h2>
						<p class="subtitle-text">
							Com apenas poucos cliques você consegue controlar de forma eficiente as entradas e saídas de seu patrimônio.
						</p>
						<a href="login.php" class="uk-button uk-button-primary uk-border-pill" data-uk-icon="arrow-right">Comece agora!</a>
					</div>
					<div data-uk-scrollspy="cls: uk-animation-fade">
						<img  data-src="imgs/marketing-2.svg" data-uk-img alt="Image">
					</div>
				</div>
			</div>
		</section>

		<section class="uk-section uk-section-default">
			
			<div class="uk-container uk-container-xsmall uk-text-center uk-section uk-padding-remove-top">
				<h2 class="uk-margin-remove uk-h1">Conheça o melhor do nosso Sistema!</h2>
			</div>
			<div class="uk-container">
				<div class="uk-grid uk-grid-large uk-child-width-1-3@m" data-uk-grid data-uk-scrollspy="target: > div; delay: 150; cls: uk-animation-slide-bottom-medium">
					<div class="uk-text-center">
						<img  data-src="imgs/marketing-3.svg" data-uk-img alt="Image">
						<h4 class="uk-margin-small-bottom uk-margin-top uk-margin-remove-adjacent">Rápido Cadastro</h4>
					</div>
					<div class="uk-text-center">
						<img  data-src="imgs/marketing-4.svg" data-uk-img alt="Image">
						<h4 class="uk-margin-small-bottom uk-margin-top uk-margin-remove-adjacent">Apresentações incriveis</h4>
					</div>
					<div class="uk-text-center">
						<img  data-src="imgs/marketing-5.svg" data-uk-img alt="Image">
						<h4 class="uk-margin-small-bottom uk-margin-top uk-margin-remove-adjacent">Suporte ao Cliente</h4>
					</div>
					<div class="uk-text-center">
						<img  data-src="imgs/marketing-2.svg" data-uk-img alt="Image">
						<h4 class="uk-margin-small-bottom uk-margin-top uk-margin-remove-adjacent">Planejamentos</h4>
					</div>
					<div class="uk-text-center">
						<img  data-src="imgs/marketing-1.svg" data-uk-img alt="Image">
						<h4 class="uk-margin-small-bottom uk-margin-top uk-margin-remove-adjacent">Grandes Análises</h4>
					</div>
					<div class="uk-text-center">
						<img  data-src="imgs/marketing-8.svg" data-uk-img alt="Image">
						<h4 class="uk-margin-small-bottom uk-margin-top uk-margin-remove-adjacent">Multiuso</h4>
					</div>
					
				</div>
			</div>
		</section>
		
		<footer class="uk-section uk-section-secondary uk-padding-remove-bottom">
			<div class="uk-container">
				<div class="uk-grid uk-grid-large" data-uk-grid>
					<div class="uk-width-1-2@m">
						<h5>CONTATOS</h5>
						<p></p>
						<div>
							<a href="" class="uk-icon-button" data-uk-icon="twitter"></a>Twitter<br><br>
							<a href="" class="uk-icon-button" data-uk-icon="facebook"></a>Facebook<br><br>
							<a href="" class="uk-icon-button" data-uk-icon="instagram"></a>Instagram
						</div>
					</div>
					<div class="uk-width-1-6@m">
						<h5>PÁGINAS</h5>
						<ul class="uk-list">
							<li>Entrar</li>
							<li>Cadastrar</li>
							<li>Sobre</li>
						</ul>
					</div>
					
					
				</div>
			</div>
			
			<div class="uk-text-center uk-padding uk-padding-remove-horizontal">
				<span class="uk-text-small uk-text-muted">© 2019 yourMoney - <a href="">Desenvolvido por Lucas Silva, Lucas Wendel e Rodrigo Silva</a> | Feito com <a href="http://getuikit.com" title="Visit UIkit 3 site" target="_blank" data-uk-tooltip><span data-uk-icon="uikit"></span></a></span>
			</div>
		</footer>
		
		<script src="js/jquery.js"></script>
		<script src="js/uikit.min.js"></script>
		<script src="js/uikit-icons.min.js"></script>
		<script src="js/wowslider.js"></script>
		<script src="js/javinha.js"></script>
	</body>
</html>
    