function maskCartao(cartao) {
	var ina = new Inputmask("9999 9999 9999 9999", {
		placeholder : "0"
	});
	ina.mask(cartao);
}
function maskValor(v) {
	alert(v);
	$("#"+v).maskMoney({
		prefix: "R$ ",
		decimal:",",
		thousands: "."
	});
}
function verificarVl(valor) {
	var valor = new Number(valor.value);
	var limite = new Number($("#limite").val());
	if (valor != 'NaN' && limite != 'NaN') {
		if (valor > limite) {
			console.log("ok");
		}
	}

}

function verificarLimite() {
	if($('#apenasDebito').prop('checked')){
		$('#limite').val("");
		document.getElementById("limite").setAttribute("disabled", "");
		$("#semConta").wrap('<span/>');
	}else{
		$('#limite').removeAttr("disabled");
		$("#semConta").unwrap();
	}
}


function mudarForma() {
	if($('#forma').val()=="carteira"){
		$('#fieldFix').hide();
		$('#fieldConta').hide();
		$('#fieldCartao').hide();
		$('#fieldTipoCartao').hide();
		$('#fieldRepeticao').hide();
		if($('#tipoRegistros').val()=="saida"){
			$('#fieldRepeticaoCarteira').show();
		}else {
			$('#fieldRepeticaoCarteira').hide();
		}
		if($('#repeticaoCarteira').val()!="fixa"){
			$('#fieldFixo').hide();
		}
		if($('#repeticaoCarteira').val()!="parcelamento"){
			$('#fieldParcelamento').hide();	
		}
		$('#fieldRepeticao').hide();
	}else if($('#forma').val()=="conta") {
		$('#fieldFix').show();
		$('#fieldCartao').hide();
		$('#fieldTipoCartao').hide();
		if($('#tipoRegistros').val()=="saida"){
			$('#fieldRepeticao').show();
		}else {
			$('#fieldRepeticao').hide();

		}
		if($('#repeticao').val()!="fixa"){
			$('#fieldFixo').hide();
		}
		if($('#repeticao').val()!="parcelamento"){
			$('#fieldParcelamento').hide();	
		}else{
			$('#fieldParcelamento').show();	
		}
		if($('#repeticao').children('option').length==2){
			$("#repetParcelamento").unwrap();	
		}
		$('#fieldConta').show();
		$('#fieldRepeticaoCarteira').hide();
	}else {
		$('#fieldFix').show();
		$('#fieldConta').hide();
		if($('#tipoRegistros').val()=="saida"){
			$('#fieldRepeticao').show();
		}else {
			$('#fieldRepeticao').hide();
		}
		if($('#repeticao').val()!="fixa"){
			$('#fieldFixo').hide();
		}
		if($('#repeticao').val()!="parcelamento"){
			$('#fieldParcelamento').hide();	
		}else{
			$('#fieldParcelamento').show();	
		}
		if($('#procedimento').val()=="debito"){
			if($('#repeticao').children('option').length==3){
				$("#repetParcelamento").wrap('<span/>');	
			}
		}
		$('#fieldRepeticaoCarteira').hide();
		$('#fieldCartao').show();
		$('#fieldTipoCartao').show();
	}
}

function mudarTipoRegistro(){
	if($('#tipoRegistros').val()=="entrada"){
		$("#formaCartao").wrap('<span/>');
		mudarForma();
	}else{
		$("#formaCartao").unwrap();
		mudarForma();
	}
}

function mudarProcedimento(){
	if($('#procedimento').val()=="credito"){
		if($('#repeticao').children('option').length==2){
			$("#repetParcelamento").unwrap();
		}
		$(".opcoesCredito").wrap('<span/>');
		mudarForma();
	}else if($('#procedimento').val()=="debito"){
		if($('#repeticao').children('option').length==3){
			$("#repetParcelamento").wrap('<span/>');
		}
		$(".opcoesCredito").unwrap();
		mudarForma();
	}
}

function mudarDataOperacao() {
	if($('#dataO').val()=="hoje"){
		$('#fieldDataOperacao').hide();
	}else{
		$('#fieldDataOperacao').show();
	}
	
}

function mudarRepeticao() {

	if($('#tipoRegistros').val()=="saida"){
		if($('#forma').val()=="carteira"){
			$('#fieldDataVencimento').hide();
		}else{

			if($('#repeticao').val()=="parcelamento"){
				if($('#parcelamento').val()=="novo"){
					$('#fieldDataVencimento').show();
				}else{
					$('#fieldDataVencimento').hide();	
				}

			}else{
				$('#fieldDataVencimento').hide();
			}
		}
		
	}else{
		$('#fieldDataVencimento').hide();
	}

}

function mudarFixo(){
	if($('#forma').val()=="carteira"){
		if($('#repeticaoCarteira').val()=="fixa"){
			$('#fieldFixo').show();
		}else{
			$('#fieldFixo').hide();
		}
	}else{
		if($('#repeticao').val()=="fixa"){
			$('#fieldFixo').show();
		}else{
			$('#fieldFixo').hide();
		}
	}

}

function mudarParcelamento(){
	if($('#forma').val()!="carteira"){
		if($('#repeticao').val()=="parcelamento"){
			$('#fieldParcelamento').show();
		}else{
			$('#fieldParcelamento').hide();
		}
	}else{
		$('#fieldParcelamento').hide();
	}

}




function mudarFiltro() {
	if($('#filtro').val()=="titulo"){
		$('#tituloField').show();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
	}else if($('#filtro').val()=="valor") {
		$('#tituloField').hide();
		$('#valorField').show();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
	}else if($('#filtro').val()=="tipoPagamento") {
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').show();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
	}
	else if($('#filtro').val()=="tipoRegistro") {
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').show();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
	}
	else if($('#filtro').val()=="dataOperacao") {
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').show();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
	}
	else if($('#filtro').val()=="dataRegistro") {
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').show();
		$('#dataFieldV').hide();
	}else if($('#filtro').val()=="dataVencimento") {
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').show();
	}else{
		$('#tituloField').hide();
		$('#valorField').hide();
		$('#tipoFieldP').hide();
		$('#tipoFieldR').hide();
		$('#dataFieldO').hide();
		$('#dataFieldR').hide();
		$('#dataFieldV').hide();
		filtro();
	}
	filtro();
}
function filtro(){
	$("#filtroForm").submit(function(e){
			e.preventDefault();
			var filtro = $("#filtro").val();
			var valor = $("#"+filtro).val();
			var idUsu = $("#idUsu").val();
			$.ajax({
				url: "Filtragem.php?filtro="+filtro+"&valor="+valor+"&idUsu="+idUsu,
				method: 'GET',
				before: function(){
					$("#recebeFiltro").val("carregando...");
				},
				success: function(resposta){
					$("#recebeFiltro").html(resposta);
					atualizarDetalhes();
				}
			})
	})
}

function deletarConta(id){
	alertify.confirm("Tem certeza que remover esta conta bancária?","Fazendo isso irá perder todos os dados de sua conta aqui no yourMoney.", function(){
		$.ajax({
			url:"apagarConta.php?id="+id,
			method: "GET",
			success:function(){
				alertify.success("Sua conta bancária foi removida!");
			}
		})
		window.location.href="contas.php";

	}, function(){});
}

function deletarCartao(id){
	alertify.confirm("Tem certeza que deseja remover este cartão?","Fazendo isso irá perder todos os dados do seu cartão aqui no yourMoney.", function(){
		$.ajax({
			url:"apagarCartao.php?id="+id,
			method: "GET",
			success:function(){
				alertify.success("Seu cartão foi removido com sucesso!");
			}
		})
		window.location.href="cartoes.php";

	}, function(){});
}

function deletarRegistro(id,forma,valor,id_forma){
	alertify.confirm("Tem certeza que deseja remover este registro?","Fazendo isso o valor do mesmo será estornado.", function(){
		$.ajax({
			url:"apagarRegistro.php?id="+id+"&forma="+forma+"&valor="+valor+"&id_forma="+id_forma,
			method: "GET",
			success:function(){
				alertify.success("Seu registro foi removido com sucesso!");
			}
		})
		window.location.href="registros.php";

	}, function(){});
}

function verRegistro(ind){
	var regDetalhes = detalhes[ind];
	alertify.myAlert(regDetalhes).resizeTo('40%','80%').setHeader('');
}

function verRegistroFiltro(ind){
	var regDetalhes = detalhes2[ind];
	alertify.myAlert(regDetalhes).resizeTo('40%','80%').setHeader('');
}

function verConta(ind){
	var contaDetalhes = detalhes[ind];
	alertify.myAlert(contaDetalhes).resizeTo('40%','80%').setHeader('');
}

function verCartao(ind){
	var cartaoDetalhes = detalhes[ind];
	alertify.myAlert(cartaoDetalhes).resizeTo('40%','80%').setHeader('');
}


function mostrarAvisos(){
	alertify.myAlert(avisos).resizeTo('40%','80%').setHeader('Avisos');	
}
