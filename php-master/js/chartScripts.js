// Chart 1
// ========================================================================


// Chart 2
// ========================================================================
var char2El = document.getElementById('chart2');

new Chart(char2El, {
  type: 'line',
  data: {
    labels: labels,
    datasets: [{
      data: carteiraData,
      label: "Carteira",
      borderColor: "#39f",
      fill: true
    }]
  },
  options: {
    maintainAspectRatio: false,
    responsiveAnimationDuration: 500,
    animation: {
      duration: 5000
    },
    title: {
      display: true,
      text: 'Sua carteira'
    },
    scales: {
       yAxes: [{
         ticks: {
           callback: function(value, index, values) {
             return value.toLocaleString("pt-BR",{style:"currency", currency:"BRL"});
           }
         }
       }]
    }
  }
});


// Chart 3
// ========================================================================
var char3El = document.getElementById('chart3');

new Chart(char3El, {
  type: 'bar',
  data: {
    labels: ["Carteira", "Contas Bancárias", "Cartões de Crédito"],
    datasets: [{
      label: "Saída mensal",
      backgroundColor: ["#39f", "#895df6", "#3cba9f", "#e8c3b9", "#c45850"],
      data: saidaTotalData
    }]
  },
  options: {
    maintainAspectRatio: false,
    responsiveAnimationDuration: 500,
    legend: {
      display: false
    },
    animation: {
      duration: 5000
    },
    title: {
      display: true,
      text: 'Saída mensal'
    },
    scales: {
       yAxes: [{
         ticks: {
           callback: function(value, index, values) {
             return value.toLocaleString("pt-BR",{style:"currency", currency:"BRL"});
           }
         }
       }]
    }
  }
});

// Chart 4
// ========================================================================
var char4El = document.getElementById('chart4');

new Chart(char4El, {
  type: 'bar',
  data: {
    labels: ["Carteira", "Contas Bancárias", "Cartões de Crédito"],
    datasets: [{
      label: "Entrada mensal",
      backgroundColor: ["#39f", "#895df6", "#3cba9f", "#e8c3b9", "#c45850"],
      data: entradaTotalData
    }]
  },
  options: {
    maintainAspectRatio: false,
    responsiveAnimationDuration: 500,
    legend: {
      display: false
    },
    animation: {
      duration: 5000
    },
    title: {
      display: true,
      text: 'Entrada mensal'
    },
    scales: {
       yAxes: [{
         ticks: {
           callback: function(value, index, values) {
             return value.toLocaleString("pt-BR",{style:"currency", currency:"BRL"});
           }
         }
       }]
    }
  }
});

var char5El = document.getElementById('chart5');
  var totalEntrada=parseFloat(entradaTotalData[0])+parseFloat(entradaTotalData[1])+parseFloat(entradaTotalData[2]);
  var totalSaida=parseFloat(saidaTotalData[0])+parseFloat(saidaTotalData[1])+parseFloat(saidaTotalData[2]);
  var valorSaida=totalSaida*(-1);
  if(totalEntrada>totalSaida){
      var b =totalEntrada-totalSaida;
      var a= "Positivo! A sobrar: R$" +b;
  }else{
      var b =totalSaida-totalEntrada;
      var a="Negativo! A dever: R$" +b;
  }
  new Chart(char5El,{
    type: 'doughnut',
    data: {
      labels: ["Entradas","Saídas"],
      datasets: [
        {
          label: "Balanço Geral",
          backgroundColor: ["#3e95cd", "#c00"],
          data: [totalEntrada,totalSaida]
        }
      ]
    },
    options: {
      responsive: true,
      maintainAspectRatio: false,
      title: {
        display: true,
        text: 'Relação Ganho/Gasto: '+a
      }
      
    }

});
