<?php
	session_start();
	date_default_timezone_set("America/Fortaleza");
	require_once("Controle/CartaoController.class.php");
	require_once("Controle/ContaBancariaController.class.php");
	require_once("Modelo/Registro.class.php");
	require_once("Controle/RegistroController.class.php");
	require_once("Modelo/Usuario.class.php");
	require_once("Controle/UsuarioController.class.php");
	$data = new DateTime();
	$usuario = unserialize($_SESSION['usuario']);
	$titulo=$_POST['titulo'];
	$valor=$_POST['valor'];
	$desc=($_POST['descricao']!="") ?$_POST['descricao'] :null;
	$repeticao = null;
	if($_POST['dataO']=="hoje") {
		$dataOperacao = $data->format("Y-m-d");
	}else {
		$dataOperacao = $_POST['dataOperacao'];
	}
	$id_Forma = null;
	$tipoCartao = null;
	$tipoRegistro=$_POST['tipoRegistros'];
	$dataRegistro = $data->format("Y-m-d H:i:s");
	$dataVencimento = null;
	$forma = $_POST['forma'];
	$fixa = (isset($_POST['fixo'])) ?$_POST['fixo'] :null;
	$parcelamento = (isset($_POST['parcelamento'])) ?$_POST['parcelamento'] :null;

	if($tipoRegistro=="saida"){
		if($forma=="carteira") {
			$repeticao = $_POST['repeticaoCarteira'];
		}else {
			$repeticao = $_POST['repeticao'];
		}
	}else {
		if($forma=="carteira") {
			$repeticao = $_POST['repeticaoCarteira'];
		}else {
			$repeticao = $_POST['repeticao'];
		}
	}

	$regC = null;
	if($repeticao=="parcelamento") {
		if($parcelamento=="novo"){
			$parcelas = "+{$_POST['dataVencimento']} months";
			$dataVencimento = new DateTime($dataOperacao);
			$dataVencimento = $dataVencimento->modify($parcelas);
			$dataVencimento = $dataVencimento->format("Y-m-d");
		}else{
			$regC = new RegistroController();
			$reg = $regC->consultarRegistrosById($parcelamento);
			$dataVencimento = new DateTime($reg->getDataVencimento());
			$dataVencimento = $dataVencimento->format("Y-m-d");
		}
	}else if($repeticao=="fixa"){
		$dataVencimento = new DateTime($dataOperacao);
		$dataVencimento = $dataVencimento->modify("+1 month");
		$dataVencimento = $dataVencimento->format("Y-m-d");
	}

	if($forma=="conta") {
		$id_Forma=$_POST['conta'];
	}else if($forma=="cartao") {
		$id_Forma=$_POST['cartao'];
		$tipoCartao=$_POST['tipoCartao'];

	}
	
	if(isset($titulo)){
		try{
			$registro=new Registro();
			$registroC=new RegistroController();
			$registro->setTitulo($titulo);
			$registro->setValor($valor);
			$registro->setDesc($desc);
			$registro->setRepeticao($repeticao);
			$registro->setTipoCartao($tipoCartao);
			$registro->setTipoPagamento($forma);
			$registro->setTipoRegistro($tipoRegistro);
			$registro->setDataRegistro($dataRegistro);
			$registro->setDataOperacao($dataOperacao);
			$registro->setDataVencimento($dataVencimento);
			$registro->setRepeticao($repeticao);
			
			if($registroC->adicionarRegistros($registro,unserialize($_SESSION['usuario']),$forma,$id_Forma)){
				$novoValor = $valor;
				if($tipoRegistro=="saida"){
					$novoValor = $valor*(-1);
				}
				if($forma=="carteira"){
					$uC = new UsuarioController();
					$uC->atualizarCarteira($usuario->getId(),$novoValor);
				}elseif($forma=="conta"){
					$contaC = new ContaBancariaController();
					$contaC->atualizarValor($id_Forma,$novoValor);
				}else{
					$cartaoC = new CartaoController();
					if($tipoCartao=="credito"){
						$cartaoC->atualizarValor($id_Forma,$novoValor);
					}else{
						$cartC = new CartaoController();
						$cart = $cartC->consultarCartoesById($id_Forma)->getContaBancaria()->getConta();
						if($cart!=null){
							$contaC = new ContaBancariaController();
							$contaC->atualizarValor($cart,$novoValor);	
						}else{
							$cartaoC->atualizarValor($id_Forma,$novoValor);
						}
						
					}
					
				}

				if($repeticao=="fixa" && $fixa!="novo"){
					$registroC->atualizarVencimento($fixa);

				}
				if($repeticao=="parcelamento" && $parcelamento!="novo"){
					$registroC->atualizarVencimento($parcelamento);
				}
				setcookie("addRegistroResult",1);
			}else{
				setcookie("addRegistroResult",0);
			}
			header("Location: registros.php");
		}catch(Exception $e){
			echo "Error: " . $e->getMessage();
		}
	}



  ?>