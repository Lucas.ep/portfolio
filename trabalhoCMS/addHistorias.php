<?php
	
	echo "<meta http-equiv='X-UA-Compatible' content='IE=edge'>";
	echo "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";	
	echo "<link rel='stylesheet' type='text/css' href='sui/semantic.css'>";
	echo "<link rel='stylesheet' type='text/css' href='css/style.css'>";
	echo "<br><center><a href='homeP.php' class='ui inverted button black'>Voltar para Home</a></center><br>";

	echo "<div class='ui form'>
					<form action='#' method='post' enctype='multipart/form-data'>
										<fieldset>
											<legend><h1 class='ui header brown'>Adicionar Historias</h1></legend><br>
									
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Título da História:</label>
												</div>
												<div class='nine wide field'>
													<input type='text' name='titulo' placeholder='Titulo Historia' required/><br>
												</div>
											</div>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Descrição da História:</label>
												</div>
												<div class='nine wide field'>
													<input type='text' name='descricao' placeholder='Descrição Historia' required/><br>
												</div>
											</div>
											<center>
												<img src='imgs/game.png' class='ui circular small image'/><br>
												<div class='ui labeled button'>
													<label for='imagem' class='ui inverted grey button tam'>Imagem</label>
												</div>
												<input type='file' id='imagem' style='display:none;' name='imagem' accept='image/*' required/>
											</center>	
											<br>
											
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Descrição da Imagem:</label>	
												</div>
												<div class='nine wide field'>
													<input type='text' name='descricao' placeholder='Descrição Historia' required/><br>
												</div>
											</div>
											<center>
												<img src='imgs/game.png' class='ui circular small image'/><br>
												<div class='ui labeled button'>
													<label for='video' class='ui inverted grey button tam'>Video</label>
												</div>
												<input type='file' id='video' style='display:none;' name='video' accept='video/*' required/>
											</center>	
											<div class='ui inverted divider'></div>
											<center><input type='submit' class='ui inverted teal large button' value='Adicionar Historia'/></center>
										</fieldset>
									</form>
								</div>
";

echo "<div id='modal' class='ui basic tiny modal'>
  			<i class='close icon'></i>
  			<div class='header'>
    			Aula adcionada com sucesso!
  			</div>         	
  
  		<div class='actions'>
    
    	<div class='ui positive right labeled icon button'>
      		Ok
      	<i class='checkmark icon'></i>
    	</div>
  	  </div>
	</div>";

	echo "<script src='js/jquery.js'></script>";
	echo "<script src='sui/semantic.js'></script>";

	function showModal(){
		echo "<script>	
				$('#modal')
				.modal('show');		
			</script>";
	}


	if(isset($_POST['titulo'])){
		try{
			require_once("Model/historias.class.php");
			require_once("Control/historiasControl.class.php");
			$h = new Historias();
			$hC = new historiasControl();
			$imagem=$_FILES['imagem'];
			$video=$_FILES['video'];
			$tipo=explode("/", $video['type']);
			$i=file_get_contents($imagem['tmp_name']);
			$v=file_get_contents($video['tmp_name']);
			$imagemB = base64_encode($i);
			$videoB=base64_encode($v);
			$h->setTitulo($_POST['titulo']);
			$h->setDescricao($_POST['descricao']);
			$h->setImagem($imagemB);
			$h->setDesimagem($_POST['desimagem']);
			$h->setVideo($videoB);
			$h->setTipov($tipo[1]);
			$r = $hC->addHistorias($h);
			if($r){
				echo "deu certo";
			}else{
				echo "deu erro";
			}
		}catch(Exception $e){
			echo "Error: " . $e->getMessage();
		}

	}

  ?>