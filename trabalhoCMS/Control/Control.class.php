<?php
require_once("Conection.class.php");
require_once("Model/Cards.class.php");

class Control{
	public function showCards(){
		$conection = new Conection("lib/mysql.ini");
		$sql="SELECT * FROM game";	
		$comando = $conection->getConection()->prepare($sql);
		$comando->execute();
		$res = $comando->fetchAll();
		$list = [];
		foreach($res as $item){
			$cards = new Cards();
			$cards->setId($item->id);
			$cards->setTitle($item->title);
			$cards->setPlatforms($item->platforms);
			$cards->setDescription($item->description);
			$cards->setPrice($item->price);
			$cards->setFiles($item->files);
			array_push($list, $cards);
		}
		$conection-> __destruct();
		return $list;
	}

	public function addCards($c){
		$conection = new Conection("lib/mysql.ini");	
		$title = $c->getTitle();
		$platforms = $c->getPlatforms();
		$description = $c->getDescription();
		$price= $c->getPrice();
		$files=$c->getFiles();
		$sql = "INSERT INTO game(title,platforms,description,price,files) VALUES(:title,:platforms,:description,:price,:file)";
		$comando = $conection->getConection()->prepare($sql);
		$comando->bindParam(':title',$title);
		$comando->bindParam(':platforms',$platforms);
		$comando->bindParam(':description',$description);
		$comando->bindParam(':price',$price);
		$comando->bindParam(':file',$files);
		if($comando->execute()){
			$conection->__destruct();
			return true;
		}else{
			$conection->__destruct();
			return false;
		}
	}
	public function pesquisa($pes){
		$conection = new Conection('lib/mysql.ini');
		$sql="SELECT * FROM game WHERE title like :ti";
		$comando=$conection->getConection()->prepare($sql);
		$comando->bindValue(":ti", "%$pes%");
		$comando->execute();
		$res=$comando->fetchAll();
		$list = [];
		foreach($res as $item){
			$cards= new Cards();
			$cards->setId($item->id);
			$cards->setTitle($item->title);
			$cards->setPlatforms($item->platforms);
			$cards->setDescription($item->description);
			$cards->setPrice($item->price);
			$cards->setFiles($item->files);
			array_push($list, $cards);
		}
		$conection-> __destruct();
		return $list;
	}
	public function deleteCards($id){
		$conection = new Conection("lib/mysql.ini");
		$sql = "DELETE FROM game WHERE id={$id}";
		$comando = $conection->getConection()->prepare($sql);
		$executar = $comando->execute();
		$conection-> __destruct();
	}

	public function selectId($id){
            $conection = new Conection("lib/mysql.ini");
            $sql = "SELECT * FROM game WHERE id=:id";
            $comando = $conection->getConection()->prepare($sql);
            $comando->bindValue(":id", $id);
            $comando->execute();
            $res = $comando->fetch();
            $cards = new Cards();
            $cards->setFiles($res->files);
            $cards->setId($res->id);
            $cards->setTitle($res->title);
            $cards->setPlatforms($res->platforms);
            $cards->setDescription($res->description);
            $cards->setPrice($res->price);
            $conection->__destruct();
            return $cards;
        }

	public function updateCard($cards){
		$conection = new Conection("lib/mysql.ini");	
        $comando = $conection->getConection()->prepare("UPDATE game SET title=:title, platforms=:platforms, description=:description, price=:price, files=:file WHERE id=:id;");
		$comando->bindValue(':title',$cards->getTitle());
		$comando->bindValue(':platforms',$cards->getPlatforms());
		$comando->bindValue(':description',$cards->getDescription());
		$comando->bindValue(':price',$cards->getPrice());
		$comando->bindValue(':file',$cards->getFiles());
		$comando->bindValue(':id',$cards->getId());
		if($comando->execute()){
			$conection->__destruct();
			return true;
		}else{
			$conection->__destruct();
			return false;
		}
	}
	




}

?>