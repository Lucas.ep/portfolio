<?php

	echo "<meta http-equiv='X-UA-Compatible' content='IE=edge'>";
	echo "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";	
	echo "<link rel='stylesheet' type='text/css' href='sui/semantic.css'>";
	echo "<link rel='stylesheet' type='text/css' href='css/style.css'>";
	echo "<br><center><a href='homeP.php' class='ui inverted button black'>Voltar para Home</a></center><br>";

			echo "<div class='ui form'>
					<form action='#' method='post' enctype='multipart/form-data'>
										<fieldset>
											<legend><h1 class='ui header brown'>Adicionar Aulas de Design</h1></legend><br>
											<center>
												<img src='imgs/game.png' class='ui circular small image'/><br>
												<div class='ui labeled button'>
													<label for='video' class='ui inverted grey button tam'>Video</label>
												</div>
												<input type='file' id='video' style='display:none;' name='videoA' accept='video/*' required/>
											</center>	
											<br>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Titulo da Aula:</label>	
												</div>
												<div class='nine wide field'>
													<input type='text' placeholder='Titulo' name='tituloA' required/>
												</div>
											</div>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Descrição da Aula:</label>
												</div>
												<div class='nine wide field'>
													<textarea rows='5' name='descriçãoA' required></textarea>
												</div>
											</div>
											<div class='ui inverted divider'></div>
											<center><input type='submit' class='ui inverted teal large button' value='Adicionar Aula'/></center>
										</fieldset>
									</form>
								</div>
";

echo "<div id='modal' class='ui basic tiny modal'>
  			<i class='close icon'></i>
  			<div class='header'>
    			Aula adcionada com sucesso!
  			</div>         	
  
  		<div class='actions'>
    
    	<div class='ui positive right labeled icon button'>
      		Ok
      	<i class='checkmark icon'></i>
    	</div>
  	  </div>
	</div>";

	echo "<script src='js/jquery.js'></script>";
	echo "<script src='sui/semantic.js'></script>";

	function showModal(){
		echo "<script>	
				$('#modal')
				.modal('show');		
			</script>";
	}

if(isset($_POST['tituloA'])){
		try{
			require_once("Model/Aulas.class.php");
			require_once("Control/aulasControl.class.php");
			$a = new Aulas();
			$aC = new aulasControl();
			$video=$_FILES['videoA'];
			$bin = file_get_contents($video['tmp_name'],$video['type']);
			$videos = base64_encode($bin);
			$a->setTitulo($_POST['tituloA']);
			$a->setDes($_POST['descriçãoA']);
			$a->setVideo($videos);
			$r = $aC->addAulas($a);
			if($r){
				showModal();			
			}else{
				echo "<script>alert('Deu errado');</script>";
			}
		}catch(Exception $e){
			echo "Error: " . $e->getMessage();
		}

	}

  ?>