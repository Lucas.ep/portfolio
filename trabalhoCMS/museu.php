<?php
	echo "

	   <div class='content'>
	   	<b class='header titulo'>INFORMAÇÕES</b><br><br>
	   	<img class='map' src='imgs/museum.png'/><br><br> 
		<div class='description cardCon'>
			O Museu da Fotografia Fortaleza é um equipamento cultural dedicado à exposição, interpretação, debate e disseminação da cultura fotográfica. Fundado e administrado pelo Instituto Paula e Silvio Frota, o MFF está aberto ao público com atividades diversas, acervo próprio, biblioteca para pesquisa, exposição permanente e exposições temporárias.<br><br>
			<p class='negrito'>CONTATO:</p>museu@museudafotografia.com.br<br>
			<p class='negrito'>TELEFONE:</p> (85)3017-3661 <br>
			<p class='negrito'>LOCALIZAÇÃO:</p> Rua Frederico Borges, 545 Varjota, Fortaleza - CE CEP 60175-040<br>
			<img class='map' src='imgs/mapa.png'/><br><br> 
			<p class='negrito'>FUNCIONAMENTO:</p> Quarta a Domingo, 12h às 17h |Entrada é grátis| cortesia Simpex e Dasart<br><br>
			<center><p class='negrito'>REDES SOCIAIS:</p><br>
			<a href='#' class='ui circular facebook icon button'><i class='facebook icon'></i></a>
			<a href='#' class='ui circular instagram icon button'><i class='instagram icon'></i></a>
			<a href='#' class='ui circular twitter icon button'><i class='twitter icon'></i></a>
			<a href='#' class='ui circular google plus icon button'><i class='google plus icon'></i></a>
			</center>
		  </div>
		</div>
	</div>";



  ?>