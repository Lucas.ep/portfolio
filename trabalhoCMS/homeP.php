<?php
	require_once("Model/Cards.class.php");
	require_once("Control/Control.class.php");
	require_once("Model/Aulas.class.php");
	require_once("Control/aulasControl.class.php");
	require_once("Model/historias.class.php");
	require_once("Control/historiasControl.class.php");
	session_start();
	echo "<meta http-equiv='X-UA-Compatible' content='IE=edge'>";
	echo "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";	
	echo "<link rel='stylesheet' type='text/css' href='sui/semantic.css'>";
	echo "<link rel='stylesheet' type='text/css' href='css/wowslider.css'>";
	echo "<link rel='stylesheet' type='text/css' href='css/style.css'>";
	echo "<div class='ui inverted sidebar vertical menu'>
				<a class='item'><img src='imgs/logotipo.png' width='200px' height='125px'/></a>";
				if (!isset($_SESSION['admin']) and !isset($_COOKIE['login'])){
					echo"<a class='item' id='requisitarAdmin'>Login</a>";
				}
				echo "
				<a href='#games1' class='item'>Noticias/Lançamentos</a>
				<a href='#games2' class='item'>Historia</a>
				<a href='#games3' class='item'>Museu do Video Game</a>
				<a href='#games4' class='item'>Primeiros Games</a>
				";
				if(isset($_SESSION['admin']) || isset($_COOKIE['login'])){
					echo "
					<a class='item' href='sair.php'>Sair</a>";
				}

	echo"
	</div>

	<div class='pusher'>

				<div class='ui fixed inverted menu'>
					      <a class='item'><button class='ui inverted grey large button' id='ativarmenu'><i class='bars icon' id='menuI'></i></button>
					      ";
					      if (isset($_SESSION['admin'])) {
						echo "
							<a href='#' class='header item'>Administrador</a>";
						}else if(isset($_COOKIE['login'])){
							echo "<a href='#' class='header item'>Usuario:{$_COOKIE['login']}</a>";
						}else{
						echo"<a href='#' class='header item'><img class='logo' src='imgs/logotipo.png'>Home</a>";
						}
					      echo "
				    <div class='right menu'>
						<div class='item'>
						    <div class='ui inverted icon input'>
						        <i class='search icon'></i>
						        <input type='text' placeholder='Pesquisar'>
						    </div>
						</div>
					</div>
				 </div><br><br>

				<div id='wowslider-container1' data-no-devices='true' data-fullscreen='true'>
				  <div id='wowslider-container'>
		                <div class='ws_images'>
		                    <ul>
		                        <li><a href='#'><img src='imgs/wold.png'  title='Conecte-se com o Mundo' id='wows_0'/></a></li>
		                        <li><a href='#'><img src='imgs/desi.jpg'  title='Aulas de Desing' id='wows_0'/></a></li>
		                        <li><a href='#'><img src='imgs/camera2.jpg'  title='Equipamentos' id='wows_0'/></a></li>
		                        <li><a href='#'><img src='imgs/image4.jpg'  title='Studios' id='wows_0'/></a></li>
		                </div>
		                <div class='ws_bullets'><div>
							<a href='#' ><span><img src='imgs/wold.jpg' /></span></a>
							<a href='#' ><span><img src='imgs/desi.jpg' /></span></a>
							<a href='#' ><span><img src='imgs/camera2.jpg' /></span></a>
							<a href='#' ><span><img src='imgs/image4.png' /></span></a>
						</div>
            	   </div>
            	   <div class='ws_shadow'></div>
            	</div><br>

            	<div class='ui inverted divider'></div>";

            	echo "<h1 class='ui header fon'>World</h1>

            	<div class='ui vertical stripe quote segment'>
		    		<div class='ui equal width stackable internally celled grid'>
		    			<div class='center aligned row'>
		        			<div class='column'>
				          		<h2 class='ui header titulo'>Fotografia</h2>
		         				<i class='ui camera massive retro icon'></i>			      
				          			<br><br>
				          			<fieldset>
										<div class='content'>			    
										  <div class='description cardCon'>Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York. </div>
										</div>
				          			</fieldset>
		        			</div>
		        			<div class='column'>
		          				<h2 class='ui header titulo'>Edição</h2>
		         				<i class='ui youtube massive retro icon'></i>			      
				          			<br><br>
				          			<fieldset>
										<div class='content'>			    
										  <div class='description cardCon'>Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York. </div>
										</div>
				          			</fieldset>
		        			</div>
		        			<div class='column'>
		        				<h2 class='ui header titulo'>Desing</h2>
		         				<i class='ui pencil alternate massive icon'></i>			      
				          			<br><br>
				          			<fieldset>
										<div class='content'>			    
										  <div class='description cardCon'>Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York.Kristy is an art director living in New York. </div>
										</div>
				          			</fieldset>
			        		</div>
		      			</div>
		      			<div class='ui inverted divider'></div>
		   			</div>
				</div>
            	";

            	echo "
  	        <div id='fundoCard'>
            	<h1 id='games1' class='ui header fon'>Lançamentos<div class='sub header fon2'>Ultimas da Semana</div></h1>
					 	";
					 
					 echo "
					 	<form action='#fundoCard' method='post'>
							<center>
							 	<div class='ui inverted icon input'>
									<i class='search icon'></i>
									<input type='text' name='pesquisa' placeholder='Pesquisar'>
								</div><br><br>
							</center>
						</form>
						";
					echo "	
            	<div class='ui vertical stripe quote segment'>
					<div class='ui equal width stackable internally grid'>
		    		 	<div class='center aligned row'>
					      		";
						$cC = new Control();
						if(isset($_POST['pesquisa'])){
							$pesq = $cC->pesquisa($_POST['pesquisa']);
						}else{
							$pesq = $cC->showCards();	
						}
						$i=0;
						if (count($pesq)==0) {
								echo "
									<div style='width:100% !important;margin:0 !important;'>

								<marquee style='color:white !important;font-size:30px;' class='ui header'>NENHUM CONTEÚDO ADICIONADO</marquee>
									
									</div>
								";
							}
						foreach ($pesq as $cards) {
							
						echo "
						<div class='column'>
							<div style='max-height:500px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis !important;' class='ui card'>
				            	<div class='image'>
				            	<div class='ui green large ribbon label'><i class='dollar icon'></i>{$cards->getPrice()}</div>
				            		<img style='max-height:200px'; src='data:image;base64,{$cards->getFiles()}'/>
			            		</div>
			            		<div class='content'>
			            			<h2 class='ui header'>{$cards->getTitle()}<div class='sub header'>{$cards->getPlatforms()}</div></h2>
			            			<div class='description'>
			            				<p>{$cards->getDescription()}</p>
			            			</div>
			            		</div>
			            		";
			            		if(isset($_SESSION['admin'])){
			            			echo "<a class='ui yellow button' href='editar.php?id={$cards->getId()}'>Editar</a>
			            			<a class='ui red button' href='apagar.php?idDel={$cards->getId()}'>Apagar</a>";
		            			}
		            			echo "
		            		</div><br>
		        		  </div>";
		        		  if($i==3 || $i==7 || $i==11 || $i==15){
		        		  	echo "</div>
		        		  	<div class='center aligned row'>
		        		  	";
		        		  	}		        		
		        		  $i++;
		        		  }
		        		echo "
		        		</div>
						</div>
					  </div>";
					  	if (!isset($_SESSION['admin'])) {
							if (isset($_COOKIE['login'])) {
								echo "<center><button class='ui teal inverted large button requisitarLogin'>Adquirir Equipamento</button></center>";
							}
						}else{
							echo "
							<br>
							<center><a href='addObjects.php' class='ui black inverted large button addG'>Adicionar Game</a>";
						}


					echo "
					</div>
				</div><br>
						";
					
						echo"
            		<div class='ui inverted divider'></div>        
            			<h1 class='ui header fon'>Histórias<div class='sub header fon2'>Desde de 1960</div></h1>
            		<div id='fundoHistoria'>
            			<div class='ui inverted divider'></div>
		            	<div class='ui vertical stripe quote segment'>
					    	<div id='games2' class='ui equal width stackable internally celled grid'>
					    		<div class='center aligned row'>";
					    		$hC=new historiasControl();
								$mostrar=$hC->showHistorias();
								$v=0;
								if (count($mostrar)==0) {
								echo "
								<br>
									<div style='width:100% !important;margin:0 !important;'>

								<marquee style='color:white !important;font-size:30px;' class='ui header'>NENHUM CONTEÚDO ADICIONADO</marquee>
									
									</div>
								";
							}
								foreach($mostrar as $historias){
								echo"
					        		<div class='column'>
					        				<h1 class='ui header fon'>{$historias->getTitulo()}</h1>
				            				<fieldset id='escolhido'>
												<div class='content'>			
												  <div class='description ajuste'>
												  	{$historias->getDescricao()}
												  </div>
												</div>
				          					</fieldset>
					        		</div>
					        		<div class='column'>
					          				<div class='ui card cardi'>
							            		<div class='image'>
							            			<img src='data:image;base64,{$historias->getImagem()}'/>
							            		</div>
							            		<div class='content'>
							            			<div class='header tituloC'>Nimrod(1951)</div>
							            			<div class='description ajuste'>
							            				{$historias->getDesimagem()}
							            			</div>
							            		</div>
							            		<div class='content a'>
							            			<div class='header tituloC'>Video Explicativo</div>
							            		</div>
							            			<video  src='data:video/{$historias->getTipov()};base64,{$historias->getVideo()}' controls width='290px'></video>
							            	</div>";
							            	if(isset($_SESSION['admin'])){
			            						echo "<a class='ui yellow button' href='editar.php?id={$historias->getId()}'>Editar</a>
			            							<a class='ui red button' href='apagarHistorias.php?idDel={$historias->getId()}'>Apagar</a>";
		            						}
					        		echo "</div>";
					        		 if($v%3==1){
		        		  				echo "</div>
		        		  						<div class='center aligned row'>
		        		  					";
		        		  				}		        		
		        		  			  $v++;
		        		  			    }
					        		echo"
					        		</div>
					      		</div>";
					      		if (isset($_SESSION['admin'])) {
								
								echo "<br><br><center><a href='addHistorias.php' class='ui black inverted large button'>Adicionar Histórias</a></center>";

								}
					      		echo "
					   		</div>
					   	</div>

					   		";
            			echo"
            				<div class='ui inverted divider'></div>

            			<div id='fundoProm'>
			            	<div class='ui vertical stripe quote segment'>
			            	<h1 id='games4' class='ui header fon'>Studios de Gravação</h1>
					    		<div class='ui equal width stackable internally celled grid'>
					    			<div class='center aligned row'>
					        			<div class='column'>
							          		<center><img class='ui big circular image' src='imgs/fon.jpg'>
							          			<h2 class='ui header titulo'>Para que serve?</h2>
							          			<fieldset id='fieldset1'>
													<div class='content'>			    
													  <div class='description primeira2'>Um estúdio de gravação é uma instalação física destinada à gravação de som. Idealmente, o espaço é projetado de forma a se obter as propriedades acústicas desejadas — difusão sonora, baixo nível de reflexões, reverberação adequada, etc. Diferentes tipos de estúdios se adequam a gravações de bandas e artistas, dublagens e sons para filmes, e mesmo a gravação de uma orquestra.</div>
													</div>
							          			</fieldset>
							          		</center>
					        			</div>
					        		</div>
					        	</div>
					        </div>		
					     </div>";
			
					     echo "
							<div class='ui inverted divider'></div>            			

							<h1 id='games4' class='ui header fon'>Aulas de Design</h1>

							<div class='ui inverted divider'></div>

							<div class='ui vertical stripe quote segment'>
						    	<div class='ui equal width stackable internally grid'>
						    		<div class='center aligned two columns row'>
						    			<div class='column'>
						    				<div class='ui equal width stackable internally two columns celled grid'>
						    		";
								    		$aC = new aulasControl();
											$comando = $aC->showAulas();
											$y=1;
											if (count($comando)==0) {
												echo "
													<div style='width:100% !important;margin:0 !important;'>

												<marquee style='color:white !important;font-size:30px;' class='ui header'>NENHUM CONTEÚDO ADICIONADO</marquee>
													
													</div>
												";
											}
											foreach ($comando as $aulas) {
								    		echo "
								        		<div class='column'>
										          	<div class='ui card pon'>
									 					<video id='videoA' style='width:100% !important;height:100% !important;' src='data:video/mp4;base64,{$aulas->getVideo()}' controls></video>
									 				</div>
								        		</div>
								        		<div class='column'>
								          			<div class='ui card vrau'>
											 			<div class='content'>
											 				<div class='header tituloC'>{$aulas->getTitulo()}</div><br>
											 				<div class='description'>
											 					<p class='conteudo'>
											 						{$aulas->getDes()}
											 					</p>
											 				</div>
											 			</div>
									 				</div>
								        		</div>
								        	</div>";
								        	if(isset($_SESSION['admin'])){
			            						echo "
			            			<a class='ui yellow button' href='#'>Editar</a>
			            			<a class='ui red button' href='apagarAulas.php?idDel={$aulas->getId()}'>Apagar</a>";
		            				}
								        echo "
								        </div>
								      		";
											if($y%2==0){
				        		  			echo "
				        		  			</div>
				        		  			<div class='center aligned two columns row'>
				        		  				<div class='column'>
								    				<div class='ui equal width stackable internally two columns grid'>

				        		  			";
				        		  			}else if($y%2==1){
				        		  			echo "
				        		  				<div class='column'>
								    				<div class='ui equal width stackable internally two columns grid'>";
				        		  			}		        		
				        		  			$y++;
				        		  			}		      		
								      		echo"
								      		</div>
									</div>
								</div>";

							echo "
							</div>
							";
							if (isset($_SESSION['admin'])) {
								
								echo "<br><center><a href='addAulas.php' class='ui black inverted large button'>Adicionar Aulas</a></center>";

								}
							echo "
						</div>
								";


						echo "
							<div class='ui divider inverted'></div>
							<center><h1 class='ui header fon'>Conheça o Museu da Fotografia<div class='sub header fon2'>Atração Artistica</div></h1>

							<div id='wowslider-container1' data-no-devices='true' data-fullscreen='true'>
							  <div id='wowslider-container'>
					                <div id='games3' class='ws_images'>
					                    <ul>
					                        <li><a href='#'><img src='imgs/museu.jpg' id='wows_0'/></a></li>
					                        <li><a href='#'><img src='imgs/museu1.jpg'  id='wows_0'/></a></li>                                                       
					                        <li><a href='#'><img src='imgs/museu4.jpg' id='wows_0'/></a></li>

					                    </ul>
					                </div>
			            	   </div>
			            	</div>

			            	<br><br><br>
			            	<button class='ui teal inverted large button requisitarMuseu'>Mais Informações</button><br></center><br>
			            	<div class='elevator-button'>
			            		<button id='elevator' class='ui inverted black circular button'><i style='margin-left:15%' class='angle double big up icon'></i></button><br><br>
			            	</div>

			            <div class='ui inverted divider'></div>

						<div class='ui inverted vertical footer segment'>
						    <div class='ui container'>
						      <div class='ui stackable inverted divided equal height stackable grid'>
						        <div class='three wide column'>
						          <h4 class='ui inverted header'>Sobre</h4>
						          <div class='ui inverted link list'>
						            <a href='#' class='item'>Desenvolvimento</a>
						            <a href='#' class='item'>Historia</a>
						            <a href='#' class='item'>Fale Conosco</a>
						          </div>
						        </div>
						        <div class='three wide column'>
						          <h4 class='ui inverted header'>Serviços</h4>
						          <div class='ui inverted link list'>
						            <a href='#' class='item'>Aprendizagem</a>
						            <a href='#' class='item'>Compra e Venda</a>
						            <a href='#' class='item'>Importações de Produtos</a>
						            <a href='#' class='item'>Prazos de Entrega</a>
						          </div>
						        </div>
						        <div class='seven wide column'>
						          <h4 class='ui inverted header'>Siga nossas redes Sociais</h4>

						          <a href='#' class='ui circular facebook icon button'><i class='facebook icon'></i></a>
						          <a href='#' class='ui circular instagram icon button'><i class='instagram icon'></i></a>
						          <a href='#' class='ui circular twitter icon button'><i class='twitter icon'></i></a>
						          <a href='#' class='ui circular google plus icon button'><i class='google plus icon'></i></a>

						        </div>
						      </div>
						    </div>
						 </div>


     
	</div>
			
		";
		
	echo "<script src='js/jquery.js'></script>";
	echo "<script src='js/ajax.js'></script>";
	echo "<script src='sui/semantic.js'></script>";
	echo "<script src='js/alert.js'></script>";
	echo "<script src='js/wowslider.js'></script>";
	echo "<script src='js/elevator.js'></script>";	
	echo "<script src='js/javinha.js'></script>";
	
	function showModal(){
		echo "<script>	
				$('#modal')
				.modal('show');		
			</script>";
	}

	
 ?>