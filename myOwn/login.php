<?php
if(!isset($noDirectLinkInitial)){
	header('location: index.php');
}
echo "
	<div class='ui inverted segment four columns grid'>
		<div class='centered column'>
			<div class='ui form'>
				<form method='post' enctype='multipart/form-data' class='vraaau'>
					<fieldset>
						<legend><h2 class='ui teal header'>Entre na sua conta</h2></legend>
						<br><br>
						<div class='fields'>
							<div class='six wide field'>
								<div class='ui large teal label fluid vrau'>Usuário: </div>
							</div>
							<div class='ten wide field'>
							 	<input type='text' placeholder='Digite aqui' name='loguser' id='loguser' required />
							</div>
						</div>
						<div class='ui inverted divider'></div>
						<div class='fields'>
							<div class='six wide field'>
								<div class='ui large teal label fluid vrau'> Senha: </div>
							</div>
							<div class='ten wide field'>
								<input type='password' placeholder='Senha' name='logpwd' id='logpwd' required />
							</div>
						</div>
						<div class='ui inverted divider'></div>
						<center>
							<input type='submit' class='ui inverted teal button' value='Entrar' id='logsubmit' />
						</center>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
";
if(isset($_POST['loguser'])){
	$noDLLog = true;
	require_once('loginBack.php');
}
?>