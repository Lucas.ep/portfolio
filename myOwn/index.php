<?php
/**
* My Ooown Images.
* Site desenvolvido por 5 jovens cursantes do 2º ano do Curso Técnico de Informática, da Escola Estadual de Educação Profissional Paulo Petrola.
* Este faz parte de um trabalho dirigido pelo professor Clemilton Lima de Sousa.
* @author Rodrigo da Silva Freitas <rodrigojato@hotmail.com>
* @author Bruno de Paiva Cruz <bruno.zpc@hotmail.com>
* @author Lucas Silva Rocha <draketmj@gmail.com>
* @author Gabriel Freire Ribeiro <bielfreireoficial@gmail.com>
* @author Rian Gomes Marinho <riangmarinho@gmail.com>
* @copyright 2018 Grupo PHP 2 Ano Informatica
* @version v.1.0
*/
echo "<link rel='stylesheet' href='css/own.css' />";
echo "<link rel='stylesheet' href='sui/semantic.min.css' />";
echo "<link rel='icon' href='imgs/iconpag.png'/>";
echo "<meta name='viewport' content='width=device-width, initial-scale=1'>";
echo "<title>My Ooown Images</title>";
/**
* Vai conferir se o há um usuário logado e, dependendo da resposta vai requisitar o login/registo ou a pagina de imagens do usuário.
*/
function checkInitialLogin(){
	if(isset($_COOKIE['login'])){
		require("gallery.php");
	}else{
		echo "
			<center><span id='logo'><h1>My Ooown Images</h1></span></center>
		";
		$noDirectLinkInitial = true;
		require_once('login.php');
		require_once('register.php');
		echo "
			<center>
			<a href='docs/index.html' class='ui inverted yellow button'>Acessar documentação</a><br>
			<span id='copyright'>©Todos os direitos reservados 2018</span>
			</center>
		";
	}
}
checkInitialLogin();
?>