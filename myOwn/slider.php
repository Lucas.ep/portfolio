<?php
/**
* Arquivo slider.php.
* É o slider utilizado para mostrar as imagens que o usuário fez o upload.
* @author Rodrigo da Silva Freitas <rodrigojato@hotmail.com>
* @author Bruno de Paiva Cruz <bruno.zpc@hotmail.com>
* @author Lucas Silva Rocha <draketmj@gmail.com>
* @author Gabriel Freire Ribeiro <bielfreireoficial@gmail.com>
* @author Rian Gomes Marinho <riangmarinho@gmail.com>
* @package trabalho
*/
/**
* Método para pegar imagens e legendas do usuário e utilizá-las no slider.
*/
function createSlider() {
	$logUser = $_COOKIE['login'];
	$dir = opendir("users/$logUser/imgs");
	$imgs = array();
	$texts = array();
	while(($file = readdir($dir))!==false){
		if($file!=="." && $file!==".." && is_file("users/$logUser/imgs/$file")){
			if(pathinfo($file, PATHINFO_EXTENSION)!="txt"){
				array_push($imgs,$file);
			}else {
				$a = fopen("users/$logUser/imgs/$file","r");
				$b = fread($a,filesize("users/$logUser/imgs/$file"));
				array_push($texts,$b);
				fclose($a);
			}
		}
	}
	closedir($dir);
	if(count($imgs)!=0){
		echo "
    	<div id='wowslider-container'>
        	<div class='ws_images'>
            	<ul>
";
					for($i=0;$i<count($imgs);$i++){
						echo "<li><img style='width:800px;height:400px' src='users/$logUser/imgs/{$imgs[$i]}' title='{$texts[$i]}' id='wows_0'/></li>";
					}
	echo "
            	</ul>
        	</div>
    	<div class='ws_bullets'>
        	<div>
";
				for($i=0;$i<count($imgs);$i++){
					echo "<a href='#'><img src='users/$logUser/imgs/{$imgs[$i]}'/>1</a>";
				}
	echo "
        	</div>
        </div>
	";
}else{
	echo "<center><div class='ui teal massive label' id='noSlider'>Sem imagens!</div></center>";
}
}	
createSlider();
?>