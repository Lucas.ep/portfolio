<?php
/**
* Arquivo uploadBack.php.
* Aqui é onde vai ocorrer o upload das imagens e do texto de legenda enviado pelo usuário.
* @author Rodrigo da Silva Freitas <rodrigojato@hotmail.com>
* @author Bruno de Paiva Cruz <bruno.zpc@hotmail.com>
* @author Lucas Silva Rocha <draketmj@gmail.com>
* @author Gabriel Freire Ribeiro <bielfreireoficial@gmail.com>
* @author Rian Gomes Marinho <riangmarinho@gmail.com>
* @package trabalho
*/
require('codeBlocks.php');
$codeBlocks = new codeBlocks();
$codeBlocks->checkLogin();
$codeBlocks->noDirectLink();
echo "
	<link rel='stylesheet' href='css/own.css'>
	<link rel='stylesheet' href='sui/semantic.min.css'>
";
/**
* Método para habilitar modal em caso de sucesso ou erro no upload de imagem e legenda.
* @param boolean $uploadModal Vai revelar se o upload foi um sucesso ou uma falha, e, dependendo da resposta, habilitará seu respectivo modal.
*/
function finalUpload($uploadModal){
	if(!$uploadModal){
		echo "
			<script>
				$('#errorUploadModal').modal('setting', 'closable', false).modal('show');
			</script>";

	}else{
		echo "
			<script>
				$('#successUploadModal').modal('setting', 'closable', false).modal('show');
			</script>";
	}
	
}
/**
* Método para efetuar upload de imagens e legendas e armazenar na pasta do usuário.
* @return boolean
*/
function uploadImage(){
	$logUser = $_COOKIE['login'];
	$image = $_FILES['img'];
	$text = $_POST['text'];
	$img = explode("/",$image['type']);
	$imgName = explode(".",$image['name']);
	if(($c = count($imgName))>2){
		for($i=1;$i<(count($imgName)-1);$i++){
			$imgName[0] = "{$imgName[0]}{$imgName[$i]}";
		}
		$imgName[1] = $imgName[$c-1];
	}
	$n=1;
	if($img[0]=="image"){
		$newImg = $image['name'];
		$newText = "{$imgName[0]}.txt";
		while(file_exists("users/$logUser/imgs/$newImg")){
			$n++;
			$newImg = "{$imgName[0]}-{$n}.{$imgName[1]}";
			$newText = "{$imgName[0]}-{$n}.txt";
		}
		move_uploaded_file($image['tmp_name'], "users/$logUser/imgs/$newImg");
		$txtFile = fopen("users/$logUser/imgs/$newText","w");
		$aText = strtoupper($text[0]);
		$bText = strtolower(substr($text,1));
		$finalText = "{$aText}{$bText}";
		fwrite($txtFile,"$finalText");
		fclose($txtFile);
		return true;
	}else{
		return false;
	}
}
echo "
	<div class='ui tiny inverted modal' id='successUploadModal'>
		<div class='ui icon header'>
			<i class='thumbs up icon'></i>
			Upload realizado com sucesso!
		</div>
		<div class='content'>
			<p><span style='color:green'>Sucesso!</span>Sua imagem e sua legenda foi enviada.</p>
		</div>
		<div class='actions'>
			<a href='upload.php' class='ui ok green button'>Ok</a>
		</div>
	</div>
	<div class='ui tiny inverted modal' id='errorUploadModal'>
		<div class='ui icon header'>
	    	<i class='thumbs down icon'></i>
	    	Erro ao efetuar upload!
	  	</div>
	  	<div class='content'>
	    	<p><span style='color:red'>Algo deu errado!</span>Para tentar resolver o problema, verifique se você enviou uma imagem.</p>
	  	</div>
	  	<div class='actions'>
	    	<a href='upload.php' class='ui ok green button'>Ok</a>
	  	</div>
	</div>
	<script src='js/jquery.js'></script>
	<script src='sui/semantic.min.js'></script>";
$successUpload = uploadImage();
finalUpload($successUpload);
?>