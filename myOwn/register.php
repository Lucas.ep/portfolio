<?php
if(!isset($noDirectLinkInitial)){
	header('location: index.php');
}
echo "
	<div class='ui inverted horizontal yellow header divider'>OU</div><br><br><br>
	<div class='ui inverted segment grid'>
		<div class='six wide centered column'>
			<div class='ui form'>
				<form method='post' enctype='multipart/form-data' class='vraaau'>
					<fieldset id='regField'>
						<legend><h2 class='ui teal header'>Cadastre-se</h2></legend>
						<br><br>
						<div class='fields'>
							<div class='four wide field'>
								<div class='ui large teal label fluid vrau'>Usuário: </div>
							</div>
							<div class='six wide field'>
							 	<input type='text' placeholder='Digite aqui' name='reguser' id='reguser' required/>
							 	<div class='ui hidden label' id='reguserror'>Por favor, digite um nome com 16 caracteres ou menos.</div>
							 	<div class='ui hidden label' id='reguserror2'>Para continuar, preencha este campo.</div>
							</div>
						</div>
						<div class='ui inverted divider'></div>					
						<div class='fields'>
							<div class='four wide field'>
								<div class='ui large teal label fluid vrau'> Senha: </div>
							</div>
							<div class='six wide field'>
							 	<input type='password' placeholder='Senha' name='regpwd' id='regpwd' required/>
							 	<div class='ui hidden label' id='regpwderror'>A senha precisa ter 8 dígitos ou mais.</div>
							 	<div class='ui hidden label' id='regpwderror2'>Para continuar, preencha este campo.</div>
							</div>
							<div class='six wide field'>
								<input type='password' placeholder='Confirmar Senha' name='regcpwd' id='regcpwd' required/>
								<div class='ui hidden label' id='regcpwderror'>As senhas estão diferentes.</div>
								<div class='ui hidden label' id='regcpwderror2'>Para continuar, preencha este campo.</div>
							</div>
						</div>
						<div class='ui inverted divider'></div>
						<div class='fields'>
							<div class='four wide field'>
								<div class='ui large teal label fluid vrau'>Email: </div>
							</div>
							<div class='twelve wide field'>
								<input type='email' placeholder='Email' name='regemail' id='regemail' required />
								<center><div class='ui hidden label' id='regemailerror'>Para continuar, preencha este campo</div></center>
							</div>
						</div>
						<div class='ui inverted divider'></div>
						<center>
							<input type='submit' class='ui inverted teal disabled button' value='Cadastrar' id='regsubmit' />
						</center>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	
";
if(isset($_POST['reguser'])){
	$noDLReg = true;
	require_once('registerBack.php');
}
echo "
	<script src='js/jquery.js'></script>
	<script src='js/reg.js'></script>
	<script src='sui/semantic.min.js'></script>";
?>