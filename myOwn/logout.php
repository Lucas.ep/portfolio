<?php
/**
* Arquivo logout.php.
* Possui método para realizar a saída da conta.
* @author Rodrigo da Silva Freitas <rodrigojato@hotmail.com>
* @author Bruno de Paiva Cruz <bruno.zpc@hotmail.com>
* @author Lucas Silva Rocha <draketmj@gmail.com>
* @author Gabriel Freire Ribeiro <bielfreireoficial@gmail.com>
* @author Rian Gomes Marinho <riangmarinho@gmail.com>
* @package trabalho
*/
require_once('codeBlocks.php');
$codeBlocks = new codeBlocks();
$codeBlocks->checkLogin();
$codeBlocks->noDirectLink();
/**
* Método para encerrar a 'sessão' (na verdade, o cookie de login).
*/
function logoutUser(){
	setcookie("login", null, 0);
	header("Location: index.php");
}
logoutUser();
?>