<?php
echo "
	<link rel='stylesheet' type='text/css' href='sui/semantic.min.css'>
	<link rel='stylesheet' type='text/css' href='css/own.css'>
	<link rel='icon' href='imgs/iconpag.png'/>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<head><title>MOI - Upar Imagens</title></head>
	<div id='fixedMenu' class='ui vertical inverted sidebar labeled icon menu'>
		<div class='item'><img src='imgs/icon.png'></div>
		<a href='gallery.php' class='item'><i class='file image outline icon'></i>Ver Imagens</a>
		<a href='#' class='active item'><i class='ui cloud upload icon'></i>Upload de Imagens</a>
		<a class='item' id='menuConfBut'><i class='cog icon'></i>Configurações da Conta</a>
		<a href='logout.php' class='item'><i class='arrow alternate circle down icon'></i>Sair</a>
	</div>
	<div class='pusher'>";
	require('codeBlocks.php');
	$codeBlocks = new codeBlocks();
	$codeBlocks->checkLogin();
	$codeBlocks->confModal();
	$codeBlocks->fixedMenu();
	$codeBlocks->delImgs('upload.php');
echo "
		<div class='ui inverted four column grid'>
			<div class='ui inverted centered column form'>
				<form action='uploadBack.php?noDirectLink=true' method='post' enctype='multipart/form-data'>
					<fieldset id='upload'>
						<center>
						<br>
						<div class='ui labeled button'>
							<label for='own' id='imgbutton' class='ui inverted teal button'><i id='iconown' class='cloud upload big icon'></i><br><br>Selecione Uma Imagem</label>
						</div>
						<input type='file' name='img' id='own' accept='image/*' style='display:none;' />
						<br><br>
						<textarea rows='3' name='text' maxlength='78' required placeholder='Digite a legenda da sua foto'></textarea><br><br>
						<input type='submit' id='upImgBut' class='ui inverted teal button' value='Enviar a foto'/>
						</center>
					</fieldset>
				</form>
			</div>
		</div>
		<center><span id='copyright'>©Todos os direitos reservados 2018</span></center>
	</div>
	
";
echo "
<script src='js/jquery.js'></script>
<script src='sui/semantic.min.js'></script>
<script type='text/javascript' src='js/own.js'></script>
</script>";
?>