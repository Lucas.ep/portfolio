<?php
/**
* Arquivo codeBlocks.php, uma classe que vai efetuar alguns métodos na utilização do site.
* @author Rodrigo da Silva Freitas <rodrigojato@hotmail.com>
* @author Bruno de Paiva Cruz <bruno.zpc@hotmail.com>
* @author Lucas Silva Rocha <draketmj@gmail.com>
* @author Gabriel Freire Ribeiro <bielfreireoficial@gmail.com>
* @author Rian Gomes Marinho <riangmarinho@gmail.com>
* @package trabalho
*/
/**
* Classe codeBlocks.
* Usada para funções que são utilizadas frequentemente no decorrer da utilização do site.
*/
class codeBlocks{
	/**
	* Verifica se houve direcionamento através de url direto para um arquivo Back-End.
	* Ele utiliza uma variável super global GET para verificar se está preenchido.
	* Caso não esteja preenchido ocorre o redirecionamento para 'index.php'.
	*/
	public function noDirectLink(){
		if(!isset($_GET['noDirectLink'])){
			header('location: index.php');
		}
	}
	/**
	* Modal em que o usuário pode optar pela configuração de senha ou pela configuração de imagens.
	* Por decorrência de um bug, foi criado um 'anti-bug' para o evitar.
	*/		
	public function confModal(){
		echo "
			<div class='ui small modal' id='modalConf'>
				<div class='header'>Escolha uma opção</div>
				<div class='actions'>
					<center>
						<div class='ui black button' id='delImgsButton'>Esvaziar galeria</div>
					</center>
				</div>
				<div class='actions'>
					<a href='#' class='antiBugModal'>anti-bug</a>" . 
					#Modal do Semantic possui bug com o primeiro link existente no mesmo, por isso o link acima.
				"	<a href='changePwd.php'><div class='ui black button'>Mudar Senha</div></a>
					<a href='#' class='antiBugModal'>anti-bug</a>
				</div>
			</div>
			<div class='ui mini modal' id='confirmDelImgs'>
				<div class='ui icon header'>
					<i class='exclamation icon'></i>
					Cuidado!
				</div>
				<div class='content'>
					<span style='color:red'>Você está prestes a apagar toda sua galeria! </span><span style='color:black'>Deseja realmente fazer isso?</span>
				</div>
				<div class='actions'>
					<div class='ui ok red button'>Não</div>
					<a href='#' class='antiBugModal'>anti-bug</a>
					<a href='changePwd.php?confirmDel=confirmDel'><div class='ui ok green button'>Sim</div></a>
				</div>
			</div>";
	}
	/**
	* Menu fixo para acessar algumas opções do site.
	*/			
	public function fixedMenu(){
		echo "
			<div class='ui inverted fixed segment menu'>
				<div class='ui inverted left right large menu'>
					<button class='ui inverted teal button big' id='ativar'><i class='home icon'></i>Menu</button>
				</div>
			</div>
			<br><br><br>
			";
	}
	/**
	* Verificar se o usuário (ainda) está logado.
	* Importante para evitar problemas de utilização e de segurança.
	*/			
	public function checkLogin(){
		if(!isset($_COOKIE['login'])) {
			header("Location: index.php");
		}
	}
	/**
	* Configurações de imagens caso esse tenha sido selecionado no modal.
	* O usuário pode apagar todas as imagens por ele upadas.
	* @param string $cuPage Variável para autorizar a execução da página, caso tenha sido feita da forma correta.
	*/			
	public function delImgs($cuPage){
		if(isset($_GET['confirmDel'])){
			if($_GET['confirmDel']=='confirmDel'){
				session_start();
				$_SESSION['delImgs'] = true;
				require('delImgs.php');
				session_destroy();
			}
		}
	}
}