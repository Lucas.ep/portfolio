let user = document.getElementById('reguser');
let pwd = document.getElementById('regpwd');
let cpwd = document.getElementById('regcpwd');
let email = document.getElementById('regemail');
let submit = document.getElementById('regsubmit');
let ue = document.getElementById('reguserror');
let ue2 = document.getElementById('reguserror2');
let pe = document.getElementById('regpwderror');
let pe2 = document.getElementById('regpwderror2');
let ce = document.getElementById('regcpwderror');
let ce2 = document.getElementById('regcpwderror2');
let ee = document.getElementById('regemailerror');
let uv, pv, cv, ev;
function confall(){
	if(uv && pv && cv && ev){
		submit.className = 'ui inverted teal button';
	}else{
		submit.className = 'ui inverted teal disabled button';
	}
}
user.addEventListener('blur', function(){
	if(user.value.length == 0){
		ue2.className = 'ui pointing red basic label';
		$('#reguserror2').transition('shake');
		uv = false;
	}else{
		$('#reguserror2').fadeOut(0);
		uv = true;
	}
	if(user.value.length > 16){
		ue.className = 'ui pointing red basic label';
		$('#reguserror').transition('shake');
		uv = false;
	}else{
		$('#reguserror').fadeOut(0);
		uv = true;
	}
	confall();
});
pwd.addEventListener('blur', function(){
	if(pwd.value.length == 0){
		pe2.className = 'ui pointing red basic label';
		$('#regpwderror2').transition('shake');
		pv = false;
	}else{
		$('#regpwderror2').fadeOut(0);
		pv = true;
	}	
	if(pwd.value.length < 8 && pwd.value.length>0){
		pe.className = 'ui pointing red basic label';
		$('#regpwderror').transition('shake');
		pv = false;
	}else{
		$('#regpwderror').fadeOut(0);
		pv = true;
	}
	if(cpwd.value != pwd.value && cpwd.value.length != 0 && pv){
		ce.className = 'ui pointing red basic label';
		$('#regcpwderror').transition('shake');
		cv = false;
	}else{
		$('#regcpwderror').fadeOut(0);
		cv = true;
	}
	confall();
});
cpwd.addEventListener('blur', function(){
	if(cpwd.value.length==0  && pv){
		ce.className = 'ui pointing red basic label';
		$('#regcpwderror').transition('shake');
		cv = false;
	}else{
		$('#regcpwderror').fadeOut(0);
		cv = true;
	}
	if(cpwd.value != pwd.value && pv){
		ce.className = 'ui pointing red basic label';
		$('#regcpwderror').transition('shake');
		cv = false;
	}else{
		$('#regcpwderror').fadeOut(0);
		cv = true;
	}
	confall();
});
email.addEventListener('blur', function(){
	if(email.value.length==0){
		ee.className = 'ui pointing red basic label';
		$('#regemailerror').transition('shake');
		ev = false;
	}else{
		$('#regemailerror').fadeOut(0);
		ev = true;
	}
});
email.addEventListener('keydown', function(){
	if(email.value.length!=0){
		$('#regemailerror').fadeOut(0);
		ev = true;
	}else{
		ev=false;
	}
	confall();
});