let pwd = document.getElementById('newPwd');
let pe = document.getElementById('changepwderror');
let pe2 = document.getElementById('changepwderror2');
let cpwd = document.getElementById('cPwd');
let ce = document.getElementById('changecpwderror');
let ce2 = document.getElementById('changecpwderror2');
let submit = document.getElementById('changesubmit')
let pv, cv;
pwd.addEventListener('blur', function(){
	if(pwd.value.length == 0){
		pe2.className = 'ui pointing red basic label';
		$('#changepwderror2').transition('shake');
		pv = false;
	}else{
		$('#changepwderror2').fadeOut(0);
		pv = true;
	}	
	if(pwd.value.length < 8 && pwd.value.length>0){
		pe.className = 'ui pointing red basic label';
		$('#changepwderror').transition('shake');
		pv = false;
	}else{
		$('#changepwderror').fadeOut(0);
		pv = true;
	}
	if(cpwd.value != pwd.value && cpwd.value.length != 0 && pv){
		ce.className = 'ui pointing red basic label';
		$('#changecpwderror').transition('shake');
		cv = false;
	}else{
		$('#changecpwderror').fadeOut(0);
		cv = true;
	}
	confall();
});
cpwd.addEventListener('blur', function(){
	if(cpwd.value.length==0  && pv){
		ce2.className = 'ui pointing red basic label';
		$('#changecpwderror2').transition('shake');
		cv = false;
	}else{
		$('#changecpwderror2').fadeOut(0);
		cv = true;
	}
	if(cpwd.value != pwd.value && pv){
		ce.className = 'ui pointing red basic label';
		$('#changecpwderror').transition('shake');
		cv = false;
	}else{
		$('#changecpwderror').fadeOut(0);
		cv = true;
	}
	confall();
});