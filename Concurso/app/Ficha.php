<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ficha extends Model
{
    protected $fillable=[
    	'linkYoutube','telefone','instrumento','cep','rua','bairro','cidade','uf','ibge','id_participante',
    ];

    public function getId(){return $this->id;}
    public function getLinkYoutube(){return $this->linkYoutube;}
    public function getTelefone(){return $this->telefone;}
    public function getInstrumento(){return $this->instrumento;}
    public function getCep(){return $this->cep;}
    public function getRua(){return $this->rua;}
    public function getBairro(){return $this->bairro;}
    public function getCidade(){return $this->cidade;}
    public function getEstado(){return $this->uf;}
    public function getIbge(){return $this->ibge;}
    public function getIdParticipante(){return $this->id_participante;}
    public function getFicha(){return $this->belongsTo('App\User', 'id_participante');}
}
