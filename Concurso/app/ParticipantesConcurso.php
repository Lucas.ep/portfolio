<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticipantesConcurso extends Model
{
	protected $fillable=[
		'id_concurso','id_participante','id_ficha','votacao',
	];

	public function getId(){return $this->id;}
	public function getIdConcurso(){return $this->id_concurso;}
	public function getIdParticipante(){return $this->id_participante;}
	public function getIdFicha(){return $this->id_ficha;}
	public function getVotacao(){return $this->votacao;}
}
