<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JuradosConcurso extends Model
{
   protected $fillable=[
   		'id_concurso','id_jurado',
   ];

   public function getId(){return $this->id;}
   public function getIdConcurso(){return $this->id_concurso;}
   public function getIdJurado(){return $this->id_jurado;}
}
