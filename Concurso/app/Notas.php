<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notas extends Model
{
   	protected $fillable=[
   		'fidelidadeE','qualidade','dificuldade','leituraP','sonoridade','presenca','precisao','musicalidade','id_participante','id_jurado','id_concurso',
   	];

   	public function getId(){return $this->id;}
   	public function getFidelidade(){return $this->fidelidadeE;}
   	public function getQualidade(){return $this->qualidade;}
   	public function getDificuldade(){return $this->dificuldade;}
   	public function getLeitura(){return $this->leituraP;}
   	public function getSonoridade(){return $this->sonoridade;}
   	public function getPresenca(){return $this->presenca;}
   	public function getPrecisao(){return $this->precisao;}
   	public function getMusicalidade(){return $this->musicalidade;}
      public function getIdParticipante(){return $this->id_participante;}
      public function getJurado(){return $this->id_jurado;}
      public function getConcurso(){return $this->id_concurso;}
      public function getNotas(){return $this->belongsTo('App\User', 'id_participante');}
}
