<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JuradosController extends Controller
{

	public function pregistrarJ(){
        return view('registrarJurados');
    }
    public function registrarJurados(Request $request){
    	\App\User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'nivel_usuario'=>$request->nivel_usuario
        ]);
        return "JuradoCadastrado";
    }
    public function mostrarJurados(){
    	$jurados=\App\User::all();
    	$juradosConcurso=\App\JuradosConcurso::all();
        return view('jurados',compact('jurados','juradosConcurso'));
    }
    public function pegarId(){
        $participantes=\Auth::User()->getId();
        return view('home' ,compact('participantes'));
    }
    public function pInscricao(){
        $participantes=\Auth::User()->getId();
        $concursos=\App\Concurso::all();
        return view('inscricao' ,compact('participantes','concursos'));
    }
}
