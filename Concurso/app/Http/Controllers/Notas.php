<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Notas extends Controller
{
    public function votar(Request $request){
    	\App\Notas::create($request->all());
    	return back();
    }
    public function pNotas(){
    	return view('mostrarNotas');
    }
    public function mostrarNotas(Request $request){
   		$notas=\App\Notas::all();
   		return view('mostrarNotas',compact('notas'));
    }
}
