<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JuradosConcurso extends Controller
{
    public function selecionarJurados(Request $request){
    	\App\JuradosConcurso::create($request->all());
    	return back();
    }

}
