<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Participantes extends Controller
{
    public function inscricao(Request $request){
    	\App\Ficha::create([
    		"linkYoutube"=>$request->linkYoutube,
    		"telefone"=>$request->telefone,
    		"instrumento"=>$request->instrumento,
    		"concurso"=>$request->concurso,
    		"cep"=>$request->cep,
    		"rua"=>$request->rua,
    		"bairro"=>$request->bairro,
    		"cidade"=>$request->cidade,
    		"uf"=>$request->uf,
    		"ibge"=>$request->ibge,
    		"id_participante"=>$request->id_participante,
    	]);

    	return "Deu certo";
    }
    public function mostrarFicha(Request $request){
    	$ficha=\App\Ficha::all();
        $participantesC=\App\ParticipantesConcurso::all();
        return view('mostrarFicha',compact('ficha','participantesC'));
    }

    public function mostrarParticipantes(){
    	$participantes=\App\User::all();
        $participantesC=\App\ParticipantesConcurso::all();
    	return view ("mostrarParticipantes", compact("participantes","participantesC"));
    }
    public function pVotar(){
        $ficha=\App\Ficha::all();
        return view("votarParticipante",compact('ficha'));
    }
    public function editarF($id){
        $fichasE=\App\Ficha::where('id_participante',$id)->get();
        $a=$fichasE->last();
        return view('editarF',compact('a'));
    }
}
