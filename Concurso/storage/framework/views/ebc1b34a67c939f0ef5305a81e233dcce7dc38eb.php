<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>

<?php $idC=$_GET['idC']; ?><br><br>
<center><table class="uk-table" border="5">
	<tr>
		<th>Participante</th>
		<th>Nome</th>
		<th>Email</th>		
		<th>Link do Youtube</th>
		<?php if(auth()->guard()->check()): ?>
			<?php if(\Auth::User()->nivel_usuario==2): ?>
				<th>Votação</th>
			<?php elseif(\Auth::User()->nivel_usuario==3): ?>
				<th>Selecionar</th>	
			<?php else: ?>
				<th>Nota</th>
			<?php endif; ?>
		<?php endif; ?>
	</tr>

<?php $__currentLoopData = $participantesC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $participantesConcurso): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php $__currentLoopData = $participantes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $participante): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>	
		<?php if($participante->getNivelUsuario()==1): ?>
			<?php if($participante->getId()==$participantesConcurso->getIdParticipante() && $participantesConcurso->getIdConcurso()==$idC): ?>
			<tr>
				<td><?php echo e($participante->getId()); ?></td>
				<td><?php echo e($participante->getName()); ?></td>
				<td><?php echo e($participante->getEmail()); ?></td>
				<td><?php echo e($participante->getFicha->getLinkYoutube()); ?></td>
				<?php if(auth()->guard()->check()): ?>
					<?php if(\Auth::User()->nivel_usuario==1): ?>
						<?php if($participante->getNotas!=null): ?>
							<td>Mostrar Notas</td>	
						<?php else: ?>
							<td>Voce nao possui nota</td>	
						<?php endif; ?>
					<?php endif; ?>
					<?php 
						$id=\Auth::User()->nivel_usuario;
						$idJ=\Auth::User()->id;
						$idP=$participante->getId();
					 ?>
				<?php endif; ?>
			<?php if(auth()->guard()->check()): ?>
				<?php if($id==2): ?>
					<?php if($participantesConcurso->getVotacao()==2): ?>
						<?php if($participante->getNotas!="[]"): ?>
							<?php $__currentLoopData = $participante->getNotas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jurado): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($jurado->getConcurso()==$idC): ?>               <?php if($jurado->getJurado()==$idJ && 	$jurado->getIdParticipante()==$idP): ?>
										<?php $a=false;break; ?>
									<?php elseif($jurado->getJurado()!=$idJ && 	$jurado->getIdParticipante()==$idP): ?>
										<?php $a=true; ?>
									<?php elseif($jurado->getJurado()!=$idJ && 	$jurado->getIdParticipante()!=$idP): ?>
										<?php $a=false; ?>
									<?php endif; ?>
								<?php else: ?>
									<?php if($jurado->getJurado()==$idJ && 	$jurado->getIdParticipante()==$idP): ?>
										<?php $a=true; ?>
									<?php elseif($jurado->getJurado()!=$idJ && 	$jurado->getIdParticipante()==$idP): ?>
										<?php $a=true; ?>
									<?php elseif($jurado->getJurado()!=$idJ && 	$jurado->getIdParticipante()!=$idP): ?>
										<?php $a=false; ?>
									<?php endif; ?>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php if(!$a): ?>
								<th><a href="<?php echo e(route('mostrarN')); ?>?id=<?php echo e($idP); ?>&idC=<?php echo e($idC); ?>">Mostrar Nota</a></th>
							<?php else: ?>
								<td><a href="<?php echo e(route('pvotar')); ?>?idC=<?php echo e($idC); ?>&id=<?php echo e($idP); ?>">Votar</a></td>
							<?php endif; ?>
						
						<?php else: ?>
							<td><a href="<?php echo e(route('pvotar')); ?>?idC=<?php echo e($idC); ?>&id=<?php echo e($idP); ?>">Votar</a></td>
						<?php endif; ?>
					<?php else: ?>
						<td>Participante Não Selecionado</td>
					<?php endif; ?>
				<?php endif; ?>
				<?php if($id==3): ?>
					<?php if($participantesConcurso->getIdConcurso()==$idC && $participantesConcurso->getIdParticipante()==$idP): ?>
						<?php 
							$idPc=$participantesConcurso->getId();
							$idV=$participantesConcurso->getVotacao();  ?>
						<?php if($idV==1): ?>
							<td><a href="<?php echo e(route('selecionarV',$idPc)); ?>">Selecionar</a></td>
						<?php else: ?>
							<td>Selecionado</td>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endif; ?>
			<?php endif; ?>
		<?php endif; ?>
		
		</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table><br>
<a href="/Concursos"><button class="uk-button uk-button-default">Voltar para Concursos</button></a></center>
<?php /* C:\Users\lucas\Desktop\laravel\laravel-master\Concurso\resources\views/mostrarParticipantes.blade.php */ ?>