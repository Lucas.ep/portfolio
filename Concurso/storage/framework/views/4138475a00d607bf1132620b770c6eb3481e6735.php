<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>
<br><br><br>

<center><form class="uk-form" action="<?php echo e(route('editarE', $concursos->getId())); ?>" style="width: 50%!important;" method="get">
	<fieldset class="uk-fieldset">
		<?php echo csrf_field(); ?>
		<label class="uk-form-label"><h4>Nome do Concurso</h4></label>
		<div class="uk-margin">
            <input class="uk-input" name="nome" type="text" placeholder="Nome do Concurso" value="<?php echo e($concursos->getNome()); ?>">
        </div>
        <label class="uk-form-label"><h4>Edital do Concurso</h4></label>
		<div class="uk-margin">
            <input class="uk-input" name="edital" type="text" placeholder="Edital do Concurso" value="<?php echo e($concursos->getEdital()); ?>">
        </div>
        <label class="uk-form-label"><h4>Data de Vencimento das Inscrições</h4></label>
		<div class="uk-margin">
            <input class="uk-input" name="dataDeVecimentoDasInscricoes" type="date" value="<?php echo e($concursos->getVencimento()); ?>" />
        </div>
        <label class="uk-form-label"><h4>Data da Final</h4><label>
		<div class="uk-margin">
            <input class="uk-input" name="dataDaFinal" type="date" value="<?php echo e($concursos->getFinal()); ?>" />
        </div><br>
        <input type="submit" class="uk-button uk-button-default" value="Editar Concurso">

		
	</fieldset>
</form></center>

<script src='js/jquery.js'></script>
<script type="text/javascript" src="js/uikit.js"></script>
<script type="text/javascript" src="js/uikit-icons.js"></script>
<?php /* /home/lucas/Desktop/Concurso/resources/views/paginaE.blade.php */ ?>