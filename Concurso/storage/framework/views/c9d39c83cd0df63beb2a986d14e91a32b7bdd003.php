        <?php $id=\Auth::User()->getId();  ?>


<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar Ficha</div>

                <div class="card-body">
                    <form method="post" action="<?php echo e(route('inscricao',$id)); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Link do Youtube:</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="linkYoutube" value="<?php echo e($a->getLinkYoutube()); ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Telefone:</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="telefone" value="<?php echo e($a->getTelefone()); ?>" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="text" class="col-md-4 col-form-label text-md-right">Instrumento:</label>

                            <div class="col-md-6">
                                 <select name="instrumento">
                                    <option value="violao">Violao</option>
                                    <option value="piano">Piano</option>
                                    <option value="acordeao">Acordeão</option>
                                    <option value="bateira">Bateira</option>
                                    <option value="violino">Violino</option>
                                    <option value="violoncelo">Violoncelo</option>
                                    <option value="flauta">Flauta</option>
                                    <option value="clarinete">Clarinete</option>
                                    <option value="cavaquinho">Cavaquinho</option>
                                    <option value="ukulele">Ukulele</option>
                                    <option value="guitarra">Guitarra</option>
                                    <option value="berimbau">Berimbau</option>
                                    <option value="saxofone">Saxofone</option>
                                </select>
                            </div>
                        </div>
                        
                        <h4>Endereço:</h4>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Cep:</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="cep" id="cep" value="<?php echo e($a->getCep()); ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Rua:</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="rua" id="rua" value="<?php echo e($a->getRua()); ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Bairro:</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="bairro" id="bairro" value="<?php echo e($a->getBairro()); ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Cidade:</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="cidade" id="cidade" value="<?php echo e($a->getCidade()); ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Estado:</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="uf" id="uf" value="<?php echo e($a->getEstado()); ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">IBGE:</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="ibge" id="ibge" value="<?php echo e($a->getIbge()); ?>" required>
                            </div>
                        </div>
                        <input type="hidden" name="id_participante" value="<?php echo e($id); ?>">
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Editar Ficha
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>
 <script type="text/javascript" >

        $(document).ready(function() {

            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#rua").val("");
                $("#bairro").val("");
                $("#cidade").val("");
                $("#uf").val("");
                $("#ibge").val("");
            }
            
            //Quando o campo cep perde o foco.
            $("#cep").blur(function() {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if(validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#rua").val("...");
                        $("#bairro").val("...");
                        $("#cidade").val("...");
                        $("#uf").val("...");
                        $("#ibge").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#rua").val(dados.logradouro);
                                $("#bairro").val(dados.bairro);
                                $("#cidade").val(dados.localidade);
                                $("#uf").val(dados.uf);
                                $("#ibge").val(dados.ibge);
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulário_cep();
                                alert("CEP não encontrado.");
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulário_cep();
                        alert("Formato de CEP inválido.");
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulário_cep();
                }
            });
        });

    </script>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/lucas/Desktop/Concurso/resources/views/editarF.blade.php */ ?>