<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>
<center><br><br>

<h4>Tabela de Notas do Participante</h4><br>	
<table class="uk-table" style="width: 50% !important;" border="1">
<tr>
	<th>Participante</th>
	<th>Jurado</th>
	<th>Concurso</th>
	<th>Fidelidade </th>
	<th>Qualidade</th>
	<th>Dificuldade</th>
	<th>Leitura de Partitura</th>
	<th>Sonoridade</th>
	<th>Presença de Palco</th>
	<th>Precisão</th>
	<th>Musicalidade</th>
	<th>Total</th>
</tr>

  	
	<?php $__currentLoopData = $notas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nota): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php 
		$idP=$nota->getIdParticipante();
		$idCp=$nota->getConcurso();
		$id=$_GET['id'];
		$idC=$_GET['idC'];
		if($idP==$id && $idC==$idCp){ ?>
		<tr>	
			<td><?php echo e($nota->getIdParticipante()); ?></td>
			<td><?php echo e($nota->getJurado()); ?></td>
			<td><?php echo e($nota->getConcurso()); ?></td>
			<td><?php echo e($nota->getFidelidade()); ?></td>
			<td><?php echo e($nota->getQualidade()); ?></td>
			<td><?php echo e($nota->getDificuldade()); ?></td>
			<td><?php echo e($nota->getLeitura()); ?></td>
			<td><?php echo e($nota->getSonoridade()); ?></td>
			<td><?php echo e($nota->getPresenca()); ?></td>
			<td><?php echo e($nota->getPrecisao()); ?></td>
			<td><?php echo e($nota->getMusicalidade()); ?></td>
			<td><?php echo e($nota->getFidelidade()+$nota->getQualidade()+$nota->getDificuldade()+$nota->getLeitura()+$nota->getSonoridade()+$nota->getPresenca()+$nota->getPrecisao()+$nota->getMusicalidade()); ?></td>
		<?php } ?>
		</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
</table><br><a href="/Concursos"><button class="uk-button uk-button-default">Voltar para Concursos</button></a></center>
<?php /* /home/lucas/Desktop/Concurso/resources/views/mostrarNotas.blade.php */ ?>