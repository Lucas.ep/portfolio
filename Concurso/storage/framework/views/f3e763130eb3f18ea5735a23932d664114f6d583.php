<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Fazer Inscrição</div>

                <div class="card-body">
                    <form method="post" action="<?php echo e(route('votar')); ?>">
                        <?php echo csrf_field(); ?>
					  	<?php if(auth()->guard()->check()): ?>
					  	<?php
					  		$id=$_GET['id'];
					  		$idC=$_GET['idC'];
					  	 	$idJ=\Auth::User()->getId();  
					  	 ?>
					  	<?php endif; ?>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Fidelidade ao Estilo:</label>

                            <div class="col-md-6">
                                <input  type="number" min="0" max="5" class="form-control" name="fidelidadeE" value="" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Qualidade:</label>

                            <div class="col-md-6">
                                <input  type="number" min="0" max="10" class="form-control" name="qualidade" value="" required>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Dificuldade:</label>

                            <div class="col-md-6">
                                <input  type="number" min="0" max="10" class="form-control" name="dificuldade" value="" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Leitura de Partitura:</label>

                            <div class="col-md-6">
                                <input  type="number" min="0" max="10" class="form-control" name="leituraP" value="" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Sonoriade:</label>

                            <div class="col-md-6">
                                <input  type="number" min="0" max="5" class="form-control" name="sonoridade" value="" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Presença de Palco:</label>

                            <div class="col-md-6">
                                <input  type="number" min="0" max="5" class="form-control" name="presenca" value="" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Precisão na Execução:</label>

                            <div class="col-md-6">
                                <input  type="number" min="0" max="10" class="form-control" name="precisao" value="" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Musicalidade:</label>

                            <div class="col-md-6">
                                <input  type="number" min="0" max="5" class="form-control" name="musicalidade" value="" required>
                            </div>
                        </div>
                        <input type="hidden" name="id_participante" value="<?php echo e($id); ?>">
						<input type="hidden" name="id_jurado" value="<?php echo e($idJ); ?>">
						<?php $__currentLoopData = $ficha; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ficha): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($ficha->getIdParticipante()==$id): ?>
							  <input type="hidden" name="id_concurso" value="<?php echo e($idC); ?>"/>
							<?php endif; ?>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Votar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/lucas/Desktop/Concurso/resources/views/votarParticipante.blade.php */ ?>