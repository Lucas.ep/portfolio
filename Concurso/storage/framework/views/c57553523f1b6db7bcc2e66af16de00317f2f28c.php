<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>

<body>   
<div>
    <div class='uk-position-relative'>
        </div>
            <div class='uk-position-top'>
                <div uk-sticky='bottom: #transparent-sticky-navbar'>
                <nav class='uk-navbar-container uk-navbar-transparent' uk-navbar id='menu'>
                      <div class="uk-navbar-center">
                        <ul class="uk-navbar-nav">
                            <li>
                                <a href='/'><img id='logu' src='imgs/logo.png'/></a>
                            </li>
                            <li>
                                <a href="/Concursos">Concursos</a>
                            </li>
                            <li>
                                <?php if(auth()->guard()->check()): ?>
                                    <a href="<?php echo e(url('/home')); ?>">Home</a>
                                <?php else: ?>
                                    <a href="<?php echo e(route('login')); ?>">Login</a>
                                <?php if(Route::has('register')): ?>
                                 <li>
                                    <a href="<?php echo e(route('register')); ?>">Register</a>
                                 </li>
                                <?php endif; ?>
                            <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                </nav>
              </div>
           </div>
        </div>
    <br><br><br><br><br><br><br><br><br>
        <center><h1 style="color: #000000;">Nossos Concursos</h1></center>
    <br><br>
    <div class="uk-child-width-1-3@s uk-text-center" uk-grid>
            <?php $__currentLoopData = $concursos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $concursos): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="uk-card uk-card-hover uk-card-body">
                        <?php $idC=$concursos->getId();
                                ?>
                        <h4 class="uk-card-title"><b>Nome: </b><?php echo e($concursos->getNome()); ?></h4>
                        <p><b>Edital: </b> <?php echo e($concursos->getEdital()); ?></p>
                        <?php $dataV=date("d/m/Y", strtotime($concursos->getVencimento()));  
                            $dataF=date("d/m/Y", strtotime($concursos->getFinal()));  
                            $dataA=date("d/m/Y");
                        ?>
                        <?php if($dataA<=$dataV): ?>
                        <p><b>Data de Vencimento das Inscrições: </b> <?php echo e($dataV); ?></p>
                        <?php else: ?>
                            <p><b>Data de Vencimento das Inscrições: </b> JÁ ENCERRADO</p>
                        <?php endif; ?>
                        <?php if($dataA<=$dataF): ?>
                            <p><b>Data da Final: </b><?php echo e($dataF); ?></p>
                        <?php else: ?>
                            <p><b>Data da Final: </b>JÁ ACONTECEU</p>
                        <?php endif; ?>
                        <?php if(auth()->guard()->check()): ?>
                            <br>
                            <?php if(\Auth::User()->nivel_usuario==3): ?>
                            <a href="<?php echo e(route('paginaE' ,$concursos->getId())); ?>"><button class="uk-button uk-button-default">Editar</button></a>
                            <a href="<?php echo e(route('apagarC', $concursos->getId())); ?>"><button class="uk-button uk-button-default">Apagar</button></a><br><br>
                            <a href="/mostrarJurados?id=<?php echo e($idC); ?>"><button class="uk-button uk-button-default">Selecionar Jurados</button></a><br><br>
                            <a class="navbar-brand" href="/mostrarParticipantes?idC=<?php echo e($idC); ?>">
                                        <button class="uk-button uk-button-default">Selecionar Participantes</button>
                                        </a>
                            <?php elseif(\Auth::User()->nivel_usuario==1): ?>
                                <br>
                                <?php 
                                $idP=\Auth::User()->getId();
                                $ficha=\App\Ficha::where('id_participante',$idP)->get()->last();
                                $idFi=$ficha->getId(); 
                                 ?>
                                <?php if($ficha!=null): ?>
                                    <?php if($mostrar!="[]"): ?>
                                      <?php $__currentLoopData = $mostrar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mostrarC): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($idC==$mostrarC->getIdConcurso() && $mostrarC->getIdParticipante()==$idP): ?>   
                                                <?php $a=true;break; ?>
                                            <?php elseif($mostrarC->getIdConcurso()!=$idC && $mostrarC->getIdParticipante()==$idP): ?>
                                                <?php $a=false;  ?>    
                                            <?php elseif($mostrarC->getIdConcurso()!=$idC && $mostrarC->getIdParticipante()!=$idP): ?>
                                                <?php $a=false;  ?>    
                                            <?php endif; ?>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                        <?php $a=false;  ?>
                                    <?php endif; ?>
                                    <?php if(!$a): ?>
                                            <?php if($dataA<$dataV && $dataA<$dataF): ?>
                                                <form action="<?php echo e(route('inscreverC')); ?>" method="post">
                                                <?php echo csrf_field(); ?>
                                                <input type="hidden" name="id_participante" value="<?php echo e($idP); ?>">
                                                <input type="hidden" name="id_concurso" value="<?php echo e($idC); ?>">
                                                <input type="hidden" name="id_ficha" value="<?php echo e($idFi); ?>">
                                                <input type="submit" value="Se Inscrever">
                                                </form>
                                            <?php else: ?>
                                                <h5>VOCÊ NÃO PODE MAIS SE INSCREVER</h5>
                                            <?php endif; ?>
                                        <?php else: ?>
                                            <a href="<?php echo e(route('mostrarFicha')); ?>?idC=<?php echo e($idC); ?>"><h4>Mostrar Ficha</h4></a>
                                        <?php endif; ?>
                                <?php else: ?>
                                    <h4>VOCÊ NÃO POSSUI UMA FICHA</h4>
                                <?php endif; ?>
                           <?php endif; ?>
                        <?php endif; ?>
                        <?php if(auth()->guard()->check()): ?>
                            <?php if(\Auth::User()->nivel_usuario==2): ?>
                                <?php $idJ=\Auth::User()->id;
                                  ?>
                                <?php if($mostrarJ!="[]"): ?>
                                    <?php $__currentLoopData = $mostrarJ; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $juradosConcurso): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($idC==$juradosConcurso->getIdConcurso() && $juradosConcurso->getIdJurado()==$idJ): ?>   
                                            <?php $b=true;break; ?>
                                        <?php elseif($juradosConcurso->getIdConcurso()!=$idC && $juradosConcurso->getIdJurado()==$idJ): ?>
                                            <?php $b=false;  ?>    
                                        <?php elseif($juradosConcurso->getIdConcurso()!=$idC && $juradosConcurso->getIdJurado()!=$idJ): ?>
                                            <?php $b=false;  ?>    
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($b): ?>
                                        <h4><a class="navbar-brand" href="/mostrarParticipantes?idC=<?php echo e($idC); ?>">
                                            Lista de Partipantes para Votação
                                            </a></h4>
                                    <?php else: ?>
                                         <h4>VOCê NÃO POSSUI PERMISSÃO PARA VOTAR NESTE CONCURSO</h4>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <h4>VOCê NÃO POSSUI PERMISSÃO PARA VOTAR NESTE CONCURSO</h4>
                                <?php endif; ?>    
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </div>
    <br><br><br><br>

    <?php if(auth()->guard()->check()): ?>
        <?php if(\Auth::User()->nivel_usuario==3): ?>
        <center><a href="/AdicionarConcurso"><button class="uk-button uk-button-default">Adicionar Concurso</button></a></center>
    <br><br>
        <?php endif; ?>
    <?php endif; ?>

</body>
<script src='js/jquery.js'></script>
<script type="text/javascript" src="js/uikit.js"></script>
<script type="text/javascript" src="js/uikit-icons.js"></script>
<?php /* /home/lucas/Desktop/Concurso/resources/views/concursos.blade.php */ ?>