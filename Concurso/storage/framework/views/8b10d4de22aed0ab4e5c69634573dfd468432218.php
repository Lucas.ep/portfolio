<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    <?php if(auth()->guard()->check()): ?>
                        <?php if(\Auth::User()->nivel_usuario==3): ?>
                            <h1>Bem Vindo Admin</h1><br>
                            <a class="navbar-brand" href="/registrarJurados">
                                Registrar Jurados
                            </a>
                            <a class="navbar-brand" href="/Concursos">Gerenciar Concursos</a>
                        <?php elseif(\Auth::User()->nivel_usuario==2): ?>
                            Bem Vindo Jurado<br><br>
                            <h2><a href="/Concursos">Mostrar Concursos</a></h2>
                        <?php else: ?>
                            <h1>Bem Vindo Participante</h1>
                            <h2><a href="/Concursos">Mostrar Concursos</a></h2>
                             <?php 
                                $id=\Auth::User()->getId();
                                $ficha=\Auth::User()->getFicha;
                              ?>
                                <?php if($ficha==null): ?>
                                    <a class="navbar-brand" href="<?php echo e(route('pinscricao',$id)); ?>"><h4>Fazer Ficha</h4></a>
                                <?php else: ?>
                                    <a href="<?php echo e(route('editarF',$id)); ?>"><h3>Editar Ficha</h3></a>
                                <?php endif; ?>
                        <?php endif; ?>   
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/lucas/Desktop/Concurso/resources/views/home.blade.php */ ?>