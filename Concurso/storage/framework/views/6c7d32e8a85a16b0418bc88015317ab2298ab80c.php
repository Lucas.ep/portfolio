<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>

<center><br><br>
<h2>SELECIONAR JURADOS</h2><br>
<table class="uk-table" border="1">
	<?php $idC=$_GET['id'];  ?>
	<tr>
		<th>Nome</th>
		<th>Email</th>
		<th>Status</th>
	</tr>
	
	<?php $__currentLoopData = $jurados; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jurados): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<?php if($jurados->nivel_usuario==2): ?>
			<tr>	
				<?php $idJ=$jurados->id;  ?>
				<td><?php echo e($jurados->getName()); ?></td>
				<td><?php echo e($jurados->getEmail()); ?></td>
				<form action="<?php echo e(route('selecionarJ')); ?>" method="post">
					<?php echo csrf_field(); ?>
					<input type="hidden" name="id_jurado" value="<?php echo e($jurados->getId()); ?>">
					<input type="hidden" name="id_concurso" value="<?php echo e($idC); ?>">
						<?php if($juradosConcurso!="[]"): ?>	
							<?php $__currentLoopData = $juradosConcurso; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $juradosCon): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<?php if($juradosCon->getIdConcurso()==$idC && $juradosCon->getIdJurado()==$idJ): ?>
										<?php $a=true;break; ?>
								<?php elseif($juradosCon->getIdConcurso()!=$idC && $juradosCon->getIdJurado()==$idJ): ?>
										<?php $a=false; ?>	
								<?php elseif($juradosCon->getIdConcurso()!=$idC && $juradosCon->getIdJurado()!=$idJ): ?>
										<?php $a=false; ?>
								<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							<?php if(!$a): ?>
								<td><input type="submit" name="" value="Selecionar"></td>
							<?php else: ?>
								<td>Jurado Selecionado</td>
							<?php endif; ?>
						<?php else: ?>
							<td><input type="submit" name="" value="Selecionar"></td>
						<?php endif; ?>
				</form>
			</tr>
		<?php endif; ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table><br><a href="/Concursos"><button class="uk-button uk-button-default">Voltar para Concursos</button></a>
</center>
<?php /* /home/lucas/Desktop/Concurso/resources/views/jurados.blade.php */ ?>