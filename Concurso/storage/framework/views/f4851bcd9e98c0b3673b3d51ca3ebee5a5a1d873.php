<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>
<br><br><br>
<center><form class="uk-form" action="<?php echo e(route('adicionando')); ?>" style="width: 50%!important;" method="post">
	<fieldset class="uk-fieldset">
		<?php echo csrf_field(); ?>
		<label class="uk-form-label"><h4>Nome do Concurso</h4></label>
		<div class="uk-margin">
            <input class="uk-input" name="nome" type="text" placeholder="Nome do Concurso">
        </div>
        <label class="uk-form-label"><h4>Edital do Concurso</h4></label>
		<div class="uk-margin">
            <input class="uk-input" name="edital" type="text" placeholder="Edital do Concurso">
        </div>
        <label class="uk-form-label"><h4>Data de Vencimento das Inscrições</h4></label>
		<div class="uk-margin">
            <input class="uk-input" name="dataDeVecimentoDasInscricoes" type="date"/>
        </div>
        <label class="uk-form-label"><h4>Data da Final</h4><label>
		<div class="uk-margin">
            <input class="uk-input" name="dataDaFinal" type="date"/>
        </div><br>
        <input type="submit" class="uk-button uk-button-default" value="Adicionar Concurso">
		
	</fieldset>
</form></center>


<script src='js/jquery.js'></script>
<script type="text/javascript" src="js/uikit.js"></script>
<script type="text/javascript" src="js/uikit-icons.js"></script>
<?php /* /home/lucas/Desktop/Concurso/resources/views/padicionar.blade.php */ ?>