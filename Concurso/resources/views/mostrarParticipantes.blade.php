<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>

<?php $idC=$_GET['idC']; ?><br><br>
<center><table class="uk-table" border="5">
	<tr>
		<th>Participante</th>
		<th>Nome</th>
		<th>Email</th>		
		<th>Link do Youtube</th>
		@auth
			@if(\Auth::User()->nivel_usuario==2)
				<th>Votação</th>
			@elseif(\Auth::User()->nivel_usuario==3)
				<th>Selecionar</th>	
			@else(\Auth::User()->nivel_usuario==1)
				<th>Nota</th>
			@endif
		@endauth
	</tr>

@foreach($participantesC as $participantesConcurso)
	@foreach($participantes as $participante)	
		@if($participante->getNivelUsuario()==1)
			@if($participante->getId()==$participantesConcurso->getIdParticipante() && $participantesConcurso->getIdConcurso()==$idC)
			<tr>
				<td>{{$participante->getId()}}</td>
				<td>{{$participante->getName()}}</td>
				<td>{{$participante->getEmail()}}</td>
				<td>{{$participante->getFicha->getLinkYoutube()}}</td>
				@auth
					@if(\Auth::User()->nivel_usuario==1)
						@if($participante->getNotas!=null)
							<td>Mostrar Notas</td>	
						@else
							<td>Voce nao possui nota</td>	
						@endif
					@endif
					<?php 
						$id=\Auth::User()->nivel_usuario;
						$idJ=\Auth::User()->id;
						$idP=$participante->getId();
					 ?>
				@endauth
			@auth
				@if($id==2)
					@if($participantesConcurso->getVotacao()==2)
						@if($participante->getNotas!="[]")
							@foreach($participante->getNotas as $jurado)
								@if($jurado->getConcurso()==$idC)               @if($jurado->getJurado()==$idJ && 	$jurado->getIdParticipante()==$idP)
										<?php $a=false;break; ?>
									@elseif($jurado->getJurado()!=$idJ && 	$jurado->getIdParticipante()==$idP)
										<?php $a=true; ?>
									@elseif($jurado->getJurado()!=$idJ && 	$jurado->getIdParticipante()!=$idP)
										<?php $a=false; ?>
									@endif
								@else
									@if($jurado->getJurado()==$idJ && 	$jurado->getIdParticipante()==$idP)
										<?php $a=true; ?>
									@elseif($jurado->getJurado()!=$idJ && 	$jurado->getIdParticipante()==$idP)
										<?php $a=true; ?>
									@elseif($jurado->getJurado()!=$idJ && 	$jurado->getIdParticipante()!=$idP)
										<?php $a=false; ?>
									@endif
								@endif
							@endforeach
							@if(!$a)
								<th><a href="{{route('mostrarN')}}?id={{$idP}}&idC={{$idC}}">Mostrar Nota</a></th>
							@else
								<td><a href="{{route('pvotar')}}?idC={{$idC}}&id={{$idP}}">Votar</a></td>
							@endif
						
						@else
							<td><a href="{{route('pvotar')}}?idC={{$idC}}&id={{$idP}}">Votar</a></td>
						@endif
					@else
						<td>Participante Não Selecionado</td>
					@endif
				@endif
				@if($id==3)
					@if($participantesConcurso->getIdConcurso()==$idC && $participantesConcurso->getIdParticipante()==$idP)
						<?php 
							$idPc=$participantesConcurso->getId();
							$idV=$participantesConcurso->getVotacao();  ?>
						@if($idV==1)
							<td><a href="{{route('selecionarV',$idPc)}}">Selecionar</a></td>
						@else
							<td>Selecionado</td>
						@endif
					@endif
				@endif
			@endif
			@endauth
		@endif
		
		</tr>
	@endforeach
@endforeach
</table><br>
<a href="/Concursos"><button class="uk-button uk-button-default">Voltar para Concursos</button></a></center>