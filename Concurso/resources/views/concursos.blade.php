<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>

<body>   
<div>
    <div class='uk-position-relative'>
        </div>
            <div class='uk-position-top'>
                <div uk-sticky='bottom: #transparent-sticky-navbar'>
                <nav class='uk-navbar-container uk-navbar-transparent' uk-navbar id='menu'>
                      <div class="uk-navbar-center">
                        <ul class="uk-navbar-nav">
                            <li>
                                <a href='/'><img id='logu' src='imgs/logo.png'/></a>
                            </li>
                            <li>
                                <a href="/Concursos">Concursos</a>
                            </li>
                            <li>
                                @auth
                                    <a href="{{ url('/home') }}">Home</a>
                                @else
                                    <a href="{{ route('login') }}">Login</a>
                                @if (Route::has('register'))
                                 <li>
                                    <a href="{{ route('register') }}">Register</a>
                                 </li>
                                @endif
                            @endauth
                            </li>
                        </ul>
                    </div>
                </nav>
              </div>
           </div>
        </div>
    <br><br><br><br><br><br><br><br><br>
        <center><h1 style="color: #000000;">Nossos Concursos</h1></center>
    <br><br>
    <div class="uk-child-width-1-3@s uk-text-center" uk-grid>
            @foreach($concursos as $concursos)
                    <div class="uk-card uk-card-hover uk-card-body">
                        <?php $idC=$concursos->getId();
                                ?>
                        <h4 class="uk-card-title"><b>Nome: </b>{{$concursos->getNome()}}</h4>
                        <p><b>Edital: </b> {{$concursos->getEdital()}}</p>
                        <?php $dataV=date("d/m/Y", strtotime($concursos->getVencimento()));  
                            $dataF=date("d/m/Y", strtotime($concursos->getFinal()));  
                            $dataA=date("d/m/Y");
                        ?>
                        @if($dataA<=$dataV)
                        <p><b>Data de Vencimento das Inscrições: </b> {{$dataV}}</p>
                        @else
                            <p><b>Data de Vencimento das Inscrições: </b> JÁ ENCERRADO</p>
                        @endif
                        @if($dataA<=$dataF)
                            <p><b>Data da Final: </b>{{$dataF}}</p>
                        @else
                            <p><b>Data da Final: </b>JÁ ACONTECEU</p>
                        @endif
                        @auth
                            <br>
                            @if(\Auth::User()->nivel_usuario==3)
                            <a href="{{route('paginaE' ,$concursos->getId())}}"><button class="uk-button uk-button-default">Editar</button></a>
                            <a href="{{route('apagarC', $concursos->getId())}}"><button class="uk-button uk-button-default">Apagar</button></a><br><br>
                            <a href="/mostrarJurados?id={{$idC}}"><button class="uk-button uk-button-default">Selecionar Jurados</button></a><br><br>
                            <a class="navbar-brand" href="/mostrarParticipantes?idC={{$idC}}">
                                        <button class="uk-button uk-button-default">Selecionar Participantes</button>
                                        </a>
                            @elseif(\Auth::User()->nivel_usuario==1)
                                <br>
                                <?php 
                                $idP=\Auth::User()->getId();
                                $ficha=\App\Ficha::where('id_participante',$idP)->get()->last();
                                $idFi=$ficha->getId(); 
                                 ?>
                                @if($ficha!=null)
                                    @if($mostrar!="[]")
                                      @foreach($mostrar as $mostrarC)
                                            @if($idC==$mostrarC->getIdConcurso() && $mostrarC->getIdParticipante()==$idP)   
                                                <?php $a=true;break; ?>
                                            @elseif($mostrarC->getIdConcurso()!=$idC && $mostrarC->getIdParticipante()==$idP)
                                                <?php $a=false;  ?>    
                                            @elseif($mostrarC->getIdConcurso()!=$idC && $mostrarC->getIdParticipante()!=$idP)
                                                <?php $a=false;  ?>    
                                            @endif
                                       @endforeach
                                    @else
                                        <?php $a=false;  ?>
                                    @endif
                                    @if(!$a)
                                            @if($dataA<$dataV && $dataA<$dataF)
                                                <form action="{{route('inscreverC')}}" method="post">
                                                @csrf
                                                <input type="hidden" name="id_participante" value="{{$idP}}">
                                                <input type="hidden" name="id_concurso" value="{{$idC}}">
                                                <input type="hidden" name="id_ficha" value="{{$idFi}}">
                                                <input type="submit" value="Se Inscrever">
                                                </form>
                                            @else
                                                <h5>VOCÊ NÃO PODE MAIS SE INSCREVER</h5>
                                            @endif
                                        @else
                                            <a href="{{route('mostrarFicha')}}?idC={{$idC}}"><h4>Mostrar Ficha</h4></a>
                                        @endif
                                @else
                                    <h4>VOCÊ NÃO POSSUI UMA FICHA</h4>
                                @endif
                           @endif
                        @endauth
                        @auth
                            @if(\Auth::User()->nivel_usuario==2)
                                <?php $idJ=\Auth::User()->id;
                                  ?>
                                @if($mostrarJ!="[]")
                                    @foreach($mostrarJ as $juradosConcurso)
                                        @if($idC==$juradosConcurso->getIdConcurso() && $juradosConcurso->getIdJurado()==$idJ)   
                                            <?php $b=true;break; ?>
                                        @elseif($juradosConcurso->getIdConcurso()!=$idC && $juradosConcurso->getIdJurado()==$idJ)
                                            <?php $b=false;  ?>    
                                        @elseif($juradosConcurso->getIdConcurso()!=$idC && $juradosConcurso->getIdJurado()!=$idJ)
                                            <?php $b=false;  ?>    
                                        @endif
                                    @endforeach
                                    @if($b)
                                        <h4><a class="navbar-brand" href="/mostrarParticipantes?idC={{$idC}}">
                                            Lista de Partipantes para Votação
                                            </a></h4>
                                    @else
                                         <h4>VOCê NÃO POSSUI PERMISSÃO PARA VOTAR NESTE CONCURSO</h4>
                                    @endif
                                @else
                                    <h4>VOCê NÃO POSSUI PERMISSÃO PARA VOTAR NESTE CONCURSO</h4>
                                @endif    
                            @endif
                        @endauth
                    </div>
            @endforeach
         </div>
    <br><br><br><br>

    @auth
        @if(\Auth::User()->nivel_usuario==3)
        <center><a href="/AdicionarConcurso"><button class="uk-button uk-button-default">Adicionar Concurso</button></a></center>
    <br><br>
        @endif
    @endauth

</body>
<script src='js/jquery.js'></script>
<script type="text/javascript" src="js/uikit.js"></script>
<script type="text/javascript" src="js/uikit-icons.js"></script>