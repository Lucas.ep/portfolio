@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    @auth
                        @if(\Auth::User()->nivel_usuario==3)
                            <h1>Bem Vindo Admin</h1><br>
                            <a class="navbar-brand" href="/registrarJurados">
                                Registrar Jurados
                            </a>
                            <a class="navbar-brand" href="/Concursos">Gerenciar Concursos</a>
                        @elseif(\Auth::User()->nivel_usuario==2)
                            Bem Vindo Jurado<br><br>
                            <h2><a href="/Concursos">Mostrar Concursos</a></h2>
                        @else
                            <h1>Bem Vindo Participante</h1>
                            <h2><a href="/Concursos">Mostrar Concursos</a></h2>
                             <?php 
                                $id=\Auth::User()->getId();
                                $ficha=\Auth::User()->getFicha;
                              ?>
                                @if($ficha==null)
                                    <a class="navbar-brand" href="{{route('pinscricao',$id)}}"><h4>Fazer Ficha</h4></a>
                                @else
                                    <a href="{{route('editarF',$id)}}"><h3>Editar Ficha</h3></a>
                                @endif
                        @endif   
                    @endauth
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
