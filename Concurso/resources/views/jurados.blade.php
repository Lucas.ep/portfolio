<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>

<center><br><br>
<h2>SELECIONAR JURADOS</h2><br>
<table class="uk-table" border="1">
	<?php $idC=$_GET['id'];  ?>
	<tr>
		<th>Nome</th>
		<th>Email</th>
		<th>Status</th>
	</tr>
	
	@foreach($jurados as $jurados)
			@if($jurados->nivel_usuario==2)
			<tr>	
				<?php $idJ=$jurados->id;  ?>
				<td>{{$jurados->getName()}}</td>
				<td>{{$jurados->getEmail()}}</td>
				<form action="{{route('selecionarJ')}}" method="post">
					@csrf
					<input type="hidden" name="id_jurado" value="{{$jurados->getId()}}">
					<input type="hidden" name="id_concurso" value="{{$idC}}">
						@if($juradosConcurso!="[]")	
							@foreach($juradosConcurso as $juradosCon)
								@if($juradosCon->getIdConcurso()==$idC && $juradosCon->getIdJurado()==$idJ)
										<?php $a=true;break; ?>
								@elseif($juradosCon->getIdConcurso()!=$idC && $juradosCon->getIdJurado()==$idJ)
										<?php $a=false; ?>	
								@elseif($juradosCon->getIdConcurso()!=$idC && $juradosCon->getIdJurado()!=$idJ)
										<?php $a=false; ?>
								@endif
							@endforeach
							@if(!$a)
								<td><input type="submit" name="" value="Selecionar"></td>
							@else
								<td>Jurado Selecionado</td>
							@endif
						@else
							<td><input type="submit" name="" value="Selecionar"></td>
						@endif
				</form>
			</tr>
		@endif
	@endforeach
</table><br><a href="/Concursos"><button class="uk-button uk-button-default">Voltar para Concursos</button></a>
</center>