<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>
<center><br><br>

<h4>Tabela de Notas do Participante</h4><br>	
<table class="uk-table" style="width: 50% !important;" border="1">
<tr>
	<th>Participante</th>
	<th>Jurado</th>
	<th>Concurso</th>
	<th>Fidelidade </th>
	<th>Qualidade</th>
	<th>Dificuldade</th>
	<th>Leitura de Partitura</th>
	<th>Sonoridade</th>
	<th>Presença de Palco</th>
	<th>Precisão</th>
	<th>Musicalidade</th>
	<th>Total</th>
</tr>

  	
	@foreach($notas as $nota)
		<?php 
		$idP=$nota->getIdParticipante();
		$idCp=$nota->getConcurso();
		$id=$_GET['id'];
		$idC=$_GET['idC'];
		if($idP==$id && $idC==$idCp){ ?>
		<tr>	
			<td>{{$nota->getIdParticipante()}}</td>
			<td>{{$nota->getJurado()}}</td>
			<td>{{$nota->getConcurso()}}</td>
			<td>{{$nota->getFidelidade()}}</td>
			<td>{{$nota->getQualidade()}}</td>
			<td>{{$nota->getDificuldade()}}</td>
			<td>{{$nota->getLeitura()}}</td>
			<td>{{$nota->getSonoridade()}}</td>
			<td>{{$nota->getPresenca()}}</td>
			<td>{{$nota->getPrecisao()}}</td>
			<td>{{$nota->getMusicalidade()}}</td>
			<td>{{$nota->getFidelidade()+$nota->getQualidade()+$nota->getDificuldade()+$nota->getLeitura()+$nota->getSonoridade()+$nota->getPresenca()+$nota->getPrecisao()+$nota->getMusicalidade()}}</td>
		<?php } ?>
		</tr>
	@endforeach	
</table><br><a href="/Concursos"><button class="uk-button uk-button-default">Voltar para Concursos</button></a></center>