<?php
if (isset($_SESSION['admin'])) {
	echo "<div id='fakeLoader'></div>";	
}

	require_once("Model/Cards.class.php");
	require_once("Control/Control.class.php");
	session_start();
	echo "<meta http-equiv='X-UA-Compatible' content='IE=edge'>";
	echo "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";	
	echo "<link rel='stylesheet' type='text/css' href='sui/semantic.css'>";
	echo "<link rel='stylesheet' type='text/css' href='css/wowslider.css'>";
	echo "<link rel='stylesheet' type='text/css' href='css/style.css'>";

	echo "<div class='ui inverted sidebar vertical menu'>
				<a class='item'><img src='imgs/logotipo.png' width='200px' height='125px'/></a>";
				if (!isset($_SESSION['admin'])) {
					echo"<a class='item' id='requisitarAdmin'>Login</a>";
				}
				echo "
				<a href='#games1' class='item'>Noticias/Lançamentos</a>
				<a href='#games2' class='item'>Historia</a>
				<a href='#games3' class='item'>Museu do Video Game</a>
				<a href='#games4' class='item'>Primeiros Games</a>
				";
				if(isset($_SESSION['admin'])){
					echo "
					<a class='item' href='sair.php'>Sair</a>";
				}

	echo"
	</div>

	<div class='pusher'>

				<div class='ui fixed inverted menu'>
					      <a class='item'><button class='ui inverted grey large button' id='ativarmenu'><i class='bars icon' id='menuI'></i></button>
					      ";
					      if (isset($_SESSION['admin'])) {
						echo "
							<a href='#' class='header item'>Administrador</a>";
						}else{
						echo"<a href='#' class='header item'><img class='logo' src='imgs/logotipo.png'>Home</a>";
						}
					      echo "
				    <div class='right menu'>
						<div class='item'>
						    <div class='ui inverted icon input'>
						        <i class='search icon'></i>
						        <input type='text' placeholder='Pesquisar'>
						    </div>
						</div>
					</div>
				 </div><br><br><br>

				<div id='wowslider-container1' data-no-devices='true' data-fullscreen='true'>
				  <div id='wowslider-container'>
		                <div class='ws_images'>
		                    <ul>
		                        <li><a href='#'><img src='imgs/his.jpg'  title='Conheça a Historia' id='wows_0'/></a></li>
		                        <li><a href='#'><img src='imgs/primeiro.jpg'  title='Primeiros Games' id='wows_0'/></a></li>
		                        <li><a href='#'><img src='imgs/melhores.jpg'  title='Melhores do momento' id='wows_0'/></a></li>
		                        <li><a href='#'><img src='imgs/image4.png'  title='Lançamentos' id='wows_0'/></a></li>
		                </div>
		                <div class='ws_bullets'><div>
							<a href='#' ><span><img src='imgs/his.jpg' /></span></a>
							<a href='#' ><span><img src='imgs/primeiro.jpg' /></span></a>
							<a href='#' ><span><img src='imgs/melhores.jpg' /></span></a>
							<a href='#' ><span><img src='imgs/image4.png' /></span></a>
						</div>
            	   </div>
            	   <div class='ws_shadow'></div>
            	</div><br>

            	<div class='ui inverted divider'></div>";

            	echo "
  	           	<div class='parallax-window' data-parallax='scroll' data-image-src='image4.jpg'>
            	<h1 id='games1' class='ui header fon'>Lançamentos<div class='sub header fon2'>Ultimas da Semana</div></h1>
            	<div class='ui inverted divider'></div>	
            	<div class='ui vertical stripe quote segment'>
					<div class='ui equal width stackable internally celled grid'>

		    		 	<div class='center aligned row'>

					      		";

						$cC = new Control();
						$comando = $cC->showCards();
						$i=0;
						foreach ($comando as $cards) {
					
						echo "
						
						<div class='column'>
							<div style='max-height:500px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis !important;' class='ui card'>
				            	<div class='image'>	
				            	<div class='ui green large ribbon label'><i class='ui dollar icon'></i>{$cards->getPrice()}</div>
				            		<img style='max-height:200px'; src='data:image;base64,{$cards->getFiles()}'/>
			            		</div>
			            		<div class='content'>
			            			<h2 class='ui header'>{$cards->getTitle()}<div class='sub header'>{$cards->getPlatforms()}</div></h2>
			            			<div class='description ajuste'>{$cards->getDescription()}</div>
			            		</div>
			            		";
			            		if(isset($_SESSION['admin'])){
			            			echo "<a class='ui yellow button' href='editar.php?id={$cards->getId()}'>Editar</a>
			            			<a class='ui red button' href='apagar.php?idDel={$cards->getId()}'>Apagar</a>";
		            			}
		            			echo "
		            		</div>
		        		  </div>";
		        		  if($i==3 || $i==7 || $i==11 || $i==15){
		        		  	echo "</div>
		        		  	<div class='center aligned row'>
		        		  	";
		        		  	}		        		
		        		  $i++;
		        		  }
		        		echo "
		        		</div>
						</div>
					  </div>					
					</div>
						";
						
			echo "</div>";

						
						if (!isset($_SESSION['admin'])) {
							if (isset($_SESSION['usuario'])) {
								echo "<br><center><button class='ui teal inverted large button requisitarLogin'>Adquirir Game</button><br></center><br>";
							}
						}else{
							echo "<center><a href='addObjects.php' class='ui green inverted large button'>Adicionar Game</a>";
						}

						echo"
            			<div class='ui inverted divider'></div>

            			<h1 class='ui header fon'>História dos Jogos<div class='sub header fon2'>Desde de 1960</div></h1>

            			<div class='ui inverted divider'></div>

		            	<div class='ui vertical stripe quote segment'>
					    	<div id='games2' class='ui equal width stackable internally celled grid'>
					    		<div class='center aligned row'>
					        		<div class='column'>
					        			<div class='ui card'>
				            				<div class='content'>
				            					<div class='header'>Inicio</div>
									 				<div class='description'>
				            							<p class='primeira'>Os jogos foram desenvolvidos para ampliar a mente e desenvolver melhor o cérebro nas atividades escolares, como uma consequência das pesquisas da computação em áreas como a inteligência artificial. A comercialização do UNIVAC I, considerado o primeiro [computador] comercial da história, em 1951 abriu caminho para a adoção dos computadores por instituições acadêmicas, órgãos de pesquisa e empresas em todo o mundo desenvolvido.
				            								____________________________________________ 
				            								Devido ao alto custo, grande consumo de energia e a necessidade de se empregar uma equipe altamente treinada para manter e operar as máquinas, a tecnologia da computação ficou inicialmente limitada às organizações maiores. Por conta disso, a criação dos primeiros jogos eletrônicos limitou-se a testes e demonstrações de teorias relacionadas a áreas como a interação humano-computador, a aprendizagem adaptativa e estratégia militar.Devido à falta da documentação de muitos desses testes, é difícil de se determinar qual teria sido o primeiro jogo eletrônico criado.</p>
				            			 	 		</div>
				            			  		</div>
		            						</div>
					        		</div>
					        		<div class='column'>
					          				<div class='ui card'>
							            		<div class='image'>
							            			<img src='imgs/nin.jpg'/>
							            		</div>
							            		<div class='content'>
							            			<div class='header'>Nimrod(1951)</div>
							            			<div class='description'>
							            				<p class='ajuste'> Desenhado por John Makepeace Bennett e construído pelo engenheiro Raymond Stuart-Williams, simulava um jogo de Nim e permitia que os participantes o jogassem contra uma inteligência artificial: o jogador pressionava botões em um painel levantado correspondente com luzes na máquina para selecionar seus movimentos, e o Nimrod movia depois, com seus cálculos representados por mais luzes.</p>
							            				</div>
							            		</div>
							            		<div class='content'>
							            			<div class='header'>Video Explicativo</div>
							            		</div>
							            		<div class='image'>
							            			<video src='imgs/vi.mp4' controls width='290px' height='230px'></video>
							            		</div>
							            	</div>
					        			</div>
					        		<div class='column'>
					          			<div class='ui card'>
				            				<div class='content'>
				            					<div class='header'>Desenvolvimento</div>
				            						<div class='description'>
				            							<p class='primeira'>Alguns dos primeiros jogos conhecidos incluem Nimrod (1951), uma máquina feita sob encomenda pela Ferranti para o Festival da Grã-Bretanha e na qual se poderia jogar o jogo matemático Nim; OXO (1952), criado por Alexander S. Douglas para o computador EDSAC e que simulava o jogo da velha; e Hutspiel (1955), um jogo de guerra construído pelo exército dos Estados Unidos para simular um conflito com a União Soviética na Europa.
															____________________________________________
															Um jogo que se destaca nesse período inicial (e também por futuramente ter sido objeto de várias disputas judiciais de patente) é Tennis for Two (1958), criado pelo físico norte-americano William Higinbotham[1] para entreter os convidados no dia da visita anual realizada pelo Laboratório Nacional de Brookhaven. Este programa simulava uma partida de tênis exibida na tela de um osciloscópio. Um ponto piscando representava a bola e os jogadores controlavam seu movimento por cima de uma linha vertical que representava a rede. Não havia na imagem a representação dos jogadores, apenas da 'bola' e da 'quadra' de tênis, numa vista lateral.</p>
				            			 	 		</div>
				            			  		</div>
		            						</div>
					        		</div>
					        		<div class='column'>
					          				<div class='ui card'>
							            		<div class='image'>
							            			<img src='imgs/tenis.jpg' id='sizeI'/>
							            		</div>
							            		<div class='content'>
							            			<div class='header'>Thenis for Two (1958)</div>
							            			<div class='description'>
							            				<p class='ajuste'>
														Tennis for Two é um jogo eletrônico de esporte desenvolvido em 1958 e que simula uma partida de tênis, sendo considerado um dos primeiros jogos eletrônicos da história.Ele foi criado para uma exposição anual no Laboratório Nacional de Brookhaven quando Higinbotham descobriu que o modelo 30 do computador analógico Donner, destinado à pesquisa governamental na instituição, poderia simular trajetórias com a resistência do vento.</div>
														</p>
							            		</div>
							            		<div class='content'>
							            			<div class='header'>Video Explicativo</div>
							            		</div>
							            		<div class='image'>
							            			<video src='imgs/tenis.mp4' controls width='290px' height='200px'></video>
							            		</div>
							            	</div>
					        			</div>
					        		</div>
					      		</div>
					   		</div><br>

							<div class='ui inverted divider'></div>

							<h1 class='ui header fon'>Conheça o Museu do Video Game<div class='sub header fon2'>Atração Artistica</div></h1>

							<div id='wowslider-container1' data-no-devices='true' data-fullscreen='true'>
							  <div id='wowslider-container'>
					                <div id='games3' class='ws_images'>
					                    <ul>
					                        <li><a href='#'><img src='imgs/museu4.jpg' id='wows_0'/></a></li>
					                        <li><a href='#'><img src='imgs/museu.jpg'  id='wows_0'/></a></li>                                                       
					                        <li><a href='#'><img src='imgs/museu1.jpg' id='wows_0'/></a></li>
					                    </ul>
			            	   </div>
			            	</div><br><br><br><center><button class='ui teal inverted large button requisitarMuseu'>Adquirir Game</button><br></center><br>
            		
            				<div class='ui inverted divider'></div>

							<h1 id='games4' class='ui header fon'>Primeiros Games</h1>

							<div class='ui inverted divider'></div>

							<div class='ui vertical stripe quote segment'>
						    	<div class='ui equal width stackable internally celled grid'>
						    		<div class='center aligned row'>
						        		<div class='column'>
								          	<div class='ui card pon'>
							 					<video src='imgs/space1.mp4' controls  class='tamanhoV'></video>
							 				</div>
						        		</div>
						        		<div class='column'>
						          			<div class='ui card vrau'>
									 			<div class='content'>
									 				<div class='header'>1962:Spacewar</div><br>
									 				<div class='description'>
									 					<p class='conteudo'>Um grupo de estudantes do MIT (Instituto Tecnológico de Massachusetts) desenvolveram, ao longo de 1961, um jogo de espaçonaves para dois jogadores, em que cada um controlava uma nave e tinha a missão de destruir a do oponente.
									 						Spacewar! tinha conceitos interessantes. As naves não eram iguais, ou seja, a opção por uma ou outra afetava o desempenho. O jogo simulava a força gravitacional de uma estrela, força que provocava desvios na trajetória dos mísseis disparados na batalha. Spacewar! é o pai da simulação física em games e dos jogos de naves.
									 					</p>
									 				</div>
									 			</div>
							 				<div>
						        		</div>
						      		</div>
						   		</div>
							</div>
						</div>


						<div class='ui inverted divider'></div>
							<div class='ui vertical stripe quote segment'>
						    	<div class='ui equal width stackable internally celled grid'>
						    		<div class='center aligned row'>
						        		<div class='column'>
								          	<div class='ui card vrau'>
												<div class='content'>
													<div class='header'>1973:Space Race</div><br>
													  <div class='description'>
														<p class='conteudo'>Este jogo, em formato arcade, foi o segundo da história da Atari. A premissa era simples: dois jogadores competiam numa disputa para levar a nave da parte de baixo da tela até o topo, com desvios dos inimigos e obstáculos pelo caminho. Space Race não teve sucesso comercial; Bushnell afirmou que era muito menos popular que Pong. A Midway afirmou que o lançamento de Space Race violou o contrato da Atari com a Asteroid, e as empresas concordaram que a Atari perderia os pagamentos de royalties pelo jogo.
															Space Race, portanto, é o vovô de todos os jogos de corrida, como Fórmula 1, Gran Turismo ou Mario Kart.
														</p>
													</div>
												</div>
											</div>
						        		</div>
						        		<div class='column'>
						          			<div class='ui card pon'>
												<div class='image'>
													<video src='imgs/space2.mp4' controls class='tamanhoV'/></video>
												</div>
											</div>
						        		</div>
						      		</div>
						   		</div>
							</div>
						</div>
						

						<div class='ui inverted divider'></div>

							<div class='ui vertical stripe quote segment'>
						    	<div class='ui equal width stackable internally celled grid'>
						    		<div class='center aligned row'>
						        		<div class='column'>
								          	<div class='ui card pon'>
							 					<video src='imgs/star.mp4' controls  class='tamanhoV'></video>
							 				</div>
						        		</div>
						        		<div class='column'>
						          			<div class='ui card vrau'>
									 			<div class='content'>
									 				<div class='header'>1971: Star Trek</div><br>
										 				<div class='description'>
														<p class='conteudo'>Em 1971, uma série de jogos surgiu, três deles com temática espacial: Star Trek, Computer Space e Galaxy Game, além de outros títulos, como Empire, The Oregon Trail e Baseball. Star Trek é o mais importante deles, pois deu início a um modelo de negócio que persiste na indústria dos jogos até hoje – a criação de jogos a partir de personagens de quadrinhos, filmes e séries de TV.
															Assim como os demais jogos dessa fase pré-console, Star Trek roda a partir de comandos textuais em computadores, instrumentos não muito acessíveis na época. No jogo, o usuário assume o controle da USS Enterprise em batalhas contra naves Klingon, que buscam invadir a galáxia.
														</p>
									 				</div>
									 			</div>
							 				</div>
						        		</div>
						      		</div><br>
						      		<div class='right aligned row'>
						      			<div class='column'>
						      				<div class='elevator-button'><button id='elevator' class='ui inverted black circular button'><i style='margin-left:15%' class='angle double big up icon'></i></button></div>
						      			</div>
						      		</div>
						   		</div>
							</div>
						</div>
						<div class='ui inverted divider'></div>

						</div>
						<div class='ui inverted vertical footer segment'>
						    <div class='ui container'>
						      <div class='ui stackable inverted divided equal height stackable grid'>
						        <div class='three wide column'>
						          <h4 class='ui inverted header'>Sobre</h4>
						          <div class='ui inverted link list'>
						            <a href='#' class='item'>Desenvolvimento</a>
						            <a href='#' class='item'>Historia</a>
						            <a href='#' class='item'>Fale Conosco</a>
						          </div>
						        </div>
						        <div class='three wide column'>
						          <h4 class='ui inverted header'>Serviços</h4>
						          <div class='ui inverted link list'>
						            <a href='#' class='item'>Aprendizagem</a>
						            <a href='#' class='item'>Compra e Venda</a>
						            <a href='#' class='item'>Importações de Produtos</a>
						            <a href='#' class='item'>Prazos de Entrega</a>
						          </div>
						        </div>
						        <div class='seven wide column'>
						          <h4 class='ui inverted header'>Siga nossas redes Sociais</h4>

						          <a href='#' class='ui circular facebook icon button'><i class='facebook icon'></i></a>
						          <a href='#' class='ui circular instagram icon button'><i class='instagram icon'></i></a>
						          <a href='#' class='ui circular twitter icon button'><i class='twitter icon'></i></a>
						          <a href='#' class='ui circular google plus icon button'><i class='google plus icon'></i></a>

						        </div>
						      </div>
						    </div>
						 </div>


     
	</div>
			
		";
		
	echo "<script src='js/jquery.js'></script>";
	echo "<script src='js/ajax.js'></script>";
	echo "<script src='sui/semantic.js'></script>";
	echo "<script src='js/alert.js'></script>";
	echo "<script src='js/wowslider.js'></script>";
	echo "<script src='js/parallax.js'></script>";
	echo"<script>
		$('.parallax-window').parallax({imageSrc: 'image4.jpg'});
	</script>";
	echo "<script src='js/fakeLoader.js'></script>";
	echo "<script type='text/javascript'>	
			$('#fakeLoader').fakeLoader({
				timeToHide:2800,
				zIndex:'500',
				bgColor:'#00000',
				imagePath:'imgs/god.jpg'
			});
		 </script>";
	echo "<script src='js/elevator.js'></script>";	
	echo "<script src='js/javinha.js'></script>";
	
 ?>