<?php
	require_once("Model/Cards.class.php");
	require_once("Control/Control.class.php");

	echo "<link rel='stylesheet' type='text/css' href='sui/semantic.css'/>";
		
	echo "<style>body{background-color:#1e1e1e;color:white}label{color:white;font-weight:bold}#imagem{width:400px !important;}form{display:inline-block;text-align:left}table{font-size:20px;border:1px solid white;width:500px;}td{border:1px solid white}.tam{font-size:medium !important;}th{border:1px solid white}</style>";

		$comando= new Control();
		$res=$comando->selectId($_GET['id']);
		echo "
						<center>
						
							<br><a href='homeP.php' class='ui inverted button black'>Voltar para Home</a><br>
							<div class='ui form'>
									<form action='#' method='post' enctype='multipart/form-data'>
										<fieldset>
											<legend><h1 class='ui header brown'>Editar</h1></legend><br>
											<center>
												<img style='max-height:250px' id='imagem' class='ui small circular image' src='data:image;base64,{$res->getFiles()}'/><br>
												<div class='ui labeled button'>
													<label for='file' onclick='document.getElementById('file').click(); return false;' class='ui inverted grey button tam'>Mudar Imagem</label>
												</div>
												<input type='file' id='file' style='display:none;' onchange='readUrl(this);' name='arquivo' accept='image/*' required/>
											</center>	
											<br>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Id:</label>
												</div>
												<div class='nine wide field'>
													<input type='text' placeholder='Id' name='id' value='{$res->getId()}' required/>
												</div>
											</div>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Nome do Game:</label>
												</div>
												<div class='nine wide field'>
													<input type='text' placeholder='Nome do Game' name='title' value='{$res->getTitle()}' required/>
												</div>
											</div>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Plataformas:</label>
												</div>
												<div class='nine wide field'>
													<input type='text' placeholder='Plataformas' name='platforms' value='{$res->getPlatforms()}' required/>
												</div>
											</div>					
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Descrição:</label>
												</div>
												<div class='nine wide field'>
													<textarea rows='2' name='description' required>{$res->getDescription()}</textarea>
												</div>
											</div>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Preço:</label>
												</div>
												<div class='nine wide field'>
													<input type='text' placeholder='Preço' name='price' value='{$res->getPrice()}' required/>
												</div>
											</div>
											<div class='ui inverted divider'></div>
											<center>
											<input type='submit' class='ui inverted green large button' value='Editar'/>
											</center>
										</fieldset>
									</form>
								</div>
								
							</center>		
	
	";

	echo "<div id='modal' class='ui basic tiny modal'>
  			<i class='close icon'></i>
  			<div class='header'>
    			Card editado com sucesso!
  			</div>         	
  
  		<div class='actions'>
    
    	<div class='ui positive right labeled icon button'>
      		Ok
      	<i class='checkmark icon'></i>
    	</div>
  	  </div>
	</div>";

	echo "<script src='js/jquery.js'></script>";
	echo "<script src='sui/semantic.js'></script>";

	function showModal1(){
		echo "<script>	
				$('#modal')
				.modal('show');		
			</script>";
	}

	if (isset($_POST['title'])) {
		try{	
			$cards = new Cards();
			$imagem=$_FILES['arquivo'];
			$ab = file_get_contents($imagem['tmp_name'],$imagem['size']);
			$foto = base64_encode($ab);
			$cards->setFiles($foto);
			$cards->setId($_POST['id']);
			$cards->setTitle($_POST['title']);
			$cards->setPlatforms($_POST['platforms']);
			$cards->setDescription($_POST['description']);
			$cards->setPrice($_POST['price']);
			$cards->setFiles($foto);
			$cC = new Control();
			$r = $cC->updateCard($cards);
			if($r){
				showModal1();			
			}else{
				echo "<script>alert('Deu errado');</script>";
			}
		}catch(Exception $e){
			echo "Error: " . $e->getMessage();
		}
	}
	echo "<script>
		function readUrl(input){
			if(input.files && input.files[0]){
				var reader= new FileReader();
				reader.onload=function(e){
					$('#imagem')
					.attr('src', e.target.result)
					.width(250)
					.heigth(200);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>";
	
?>