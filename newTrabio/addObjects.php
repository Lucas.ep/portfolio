<?php

	echo "<meta http-equiv='X-UA-Compatible' content='IE=edge'>";
	echo "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";	
	echo "<link rel='stylesheet' type='text/css' href='sui/semantic.css'>";
	echo "<link rel='stylesheet' type='text/css' href='css/style.css'>";

	echo "<br><center><a href='homeP.php' class='ui inverted button black'>Voltar para Home</a></center><br>

			<div class='ui vertical stripe quote segment'>
			    	<div class='ui equal width stackable internally celled grid'>
			    		<div class='center aligned row'>
			        		<div class='column'>
					          	<div class='ui form'>
									<form action='#' method='post' enctype='multipart/form-data'>
										<fieldset>
											<legend><h1 class='ui header brown'>Adicionar Cards</h1></legend><br>
											<center>
												<img id='imagem' src='imgs/game.png' class='ui circular small image'/><br>
												<div class='ui labeled button'>
													<label for='file' onclick='document.getElementById('file').click(); return false;' class='ui inverted grey button tam'>Imagem do Game</label>
												</div>
												<input type='file' id='file' style='display:none;' onchange='readUrl(this);' name='arquivo' accept='image/*' required/>
											</center>	
											<br>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Nome do Game:</label>
												</div>
												<div class='nine wide field'>
													<input type='text' placeholder='Nome do Game' name='title' required/>
												</div>
											</div>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Plataformas:</label>
												</div>
												<div class='nine wide field'>
													<input type='text' placeholder='Plataformas' name='platforms' required/>
												</div>
											</div>					
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Descrição:</label>
												</div>
												<div class='nine wide field'>
													<textarea rows='2' name='description' required></textarea>
												</div>
											</div>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Preço:</label>
												</div>
												<div class='nine wide field'>
													<input type='text' placeholder='Preço' name='price' required/>
												</div>
											</div>
											<div class='ui inverted divider'></div>
											<center>
											<input type='submit' class='ui inverted green large button' value='Adicionar'/>
											</center>
										</fieldset>
									</form>
								</div>
			        		</div>

		        			<div class='column'>
		          				<div class='ui form'>
									<form action='#' method='post' enctype='multipart/from-data'>
										<fieldset>
											<legend><h1 class='ui header brown'>Adicionar Historias e Videos</h1></legend><br>
											<center>
												<img src='imgs/game.png' class='ui circular small image'/><br>
												<div class='ui labeled button'>
													<label for='video' class='ui inverted grey button tam'>Video</label>
												</div>
												<input type='file' id='video' style='display:none;' name='nameV' accept='image/*' required/>
											</center>	
											<br>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Titulo:</label>	
												</div>
												<div class='nine wide field'>
													<input type='text' placeholder='Titulo' name='nameTitulo' required/>
												</div>
											</div>
											<div class='fields'>
												<div class='seven wide field'>
													<label class='ui label grey tam'>Descrição da Historia:</label>
												</div>
												<div class='nine wide field'>
													<textarea rows='5' name='nameD' required></textarea>
												</div>
											</div>
											<div class='ui inverted divider'></div>
											<center><input type='submit' class='ui inverted teal large button' value='Adicionar Historia'/></center>
										</fieldset>
									</form>
								</div>
		        			</div>

		      			</div>
		   			</div>
				</div>
		
	";

	echo "<div id='modal' class='ui basic tiny modal'>
  			<i class='close icon'></i>
  			<div class='header'>
    			Game adcionado com sucesso!
  			</div>         	
  
  		<div class='actions'>
    
    	<div class='ui positive right labeled icon button'>
      		Ok
      	<i class='checkmark icon'></i>
    	</div>
  	  </div>
	</div>";

	echo "<script src='js/jquery.js'></script>";
	echo "<script src='sui/semantic.js'></script>";

	function showModal(){
		echo "<script>	
				$('#modal')
				.modal('show');		
			</script>";
	}

	if(isset($_POST['title'])){
		try{
			require_once("Model/Cards.class.php");
			require_once("Control/Control.class.php");
			$c = new Cards();
			$cC = new Control();
			$imagem=$_FILES['arquivo'];
			echo "<script>alert({$imagem['type']})</script>";
			$a = file_get_contents($imagem['tmp_name'],$imagem['size']);
			$foto = base64_encode($a);
			$c->setTitle($_POST['title']);
			$c->setPlatforms($_POST['platforms']);
			$c->setDescription($_POST['description']);
			$c->setPrice($_POST['price']);
			$c->setFiles($foto);
			$r = $cC->addCards($c);
			if($r){
				showModal();			
			}else{
				echo "<script>alert('Deu errado');</script>";
			}
		}catch(Exception $e){
			echo "Error: " . $e->getMessage();
		}

	}


	echo "<script>
		function readUrl(input){
			if(input.files && input.files[0]){
				var reader= new FileReader();
				reader.onload=function(e){
					$('#imagem')
					.attr('src', e.target.result)
					.width(250)
					.heigth(200);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>";

  ?>